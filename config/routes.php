<?php

return array(
    'product/([a-z]+)' => 'product/view/$1', 
      '/ru/product/([a-z]+)' => 'product/view/$1', // actionView в ProductController
    // actionView в ProductController
    'product/addAjaxcomment/([a-z]+)' => 'product/addAjaxcomment/$1',
    'category/([a-z]+)' => 'catalog/category/$1', // actionCategory в CatalogController
    'articles/([a-z]+)/([a-z]+)' => 'articles/view/$1/$2',
    'content/([a-z]+)' => 'pages/view/$1',
    'cart/checkout' => 'cart/checkout',
    'cart/delete/([0-9]+)' => 'cart/delete/$1',
    'cart/add/([0-9]+)' => 'cart/add/$1',
    'cart/addAjax/([0-9]+)' => 'cart/addAjax/$1',
    'cart/searchAjax/([a-z]+)' => 'cart/searchAjax/$1',
    'cart' => 'cart/index','user/register' => 'user/register',
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    'cabinet/edit' => 'cabinet/edit',
    'cabinet' => 'cabinet/index',
    'admin/ask' => 'adminAsk/index/$1',
    'admin/products/create' => 'adminProduct/create',
    'admin/products/update/([0-9]+)' => 'adminProduct/update/$1',
    'admin/products/delete/([0-9]+)' => 'adminProduct/delete/$1',
    'admin/product' => 'adminProduct/index',
    'admin/articles/create' => 'adminArticles/create',
    'admin/articles/update/([0-9]+)' => 'adminArticles/update/$1',
    'admin/articles/delete/([0-9]+)' => 'adminArticles/delete/$1',
    'admin/articles' => 'adminArticles/index',
    'admin/pages/create' => 'adminPages/create',
    'admin/pages/update/([0-9]+)' => 'adminPages/update/$1',
    'admin/pages/delete/([0-9]+)' => 'adminPages/delete/$1',
    'admin/pages' => 'adminPages/index',
    'admin/setting' => 'adminSetting/index',
    'admin/vidhuk/update/([0-9]+)' => 'adminVidhuk/update/$1',
    'admin/vidhuk/delete/([0-9]+)' => 'adminVidhuk/delete/$1',
    'admin/vidhuk' => 'adminVidhuk/index',

    //'vidhuki/page-([0-9]+)' => 'site/index/$1', // actionCategory в CatalogController
   //'vidhuki/' => 'site/vidhuki/', // actionCategory в CatalogController
    // 'admin/setting' => 'adminSetting/updates',
    // Управление Фотоматіалами:
//    'admin/gallery/create' => 'adminArticles/create',
//    'admin/gallery/update/([0-9]+)' => 'adminGallery/update/$1',
//    'admin/gallery/delete/([0-9]+)' => 'adminGallery/delete/$1',
    
    'admin/photomaterial/create' => 'adminGallery/create',
     'admin/photomaterial/update/([0-9]+)' => 'adminGallery/update/$1',
    'admin/photomaterial' => 'adminGallery/index',
    // Управление категориями:
    'admin/category/create' => 'adminCategory/create',
    'admin/category/update/([0-9]+)' => 'adminCategory/update/$1',
    'admin/category/delete/([0-9]+)' => 'adminCategory/delete/$1',
    'admin/category' => 'adminCategory/index',
    // Управление заказами:
    'admin/order/update/([0-9]+)' => 'adminOrder/update/$1',
    'admin/order/delete/([0-9]+)' => 'adminOrder/delete/$1',
    'admin/order/view/([0-9]+)' => 'adminOrder/view/$1',
    'admin/order' => 'adminOrder/index',
    // Админпанель:

    'admin' => 'admin/index',
    // О магазине
    'ask-a-question'=>'site/ask',
    'feedback' => 'site/feedback',
    'about' => 'site/about',
    'vidhuki/page-([0-9]+)' => 'vidhuki/index/$1',
    '^(ua)/vidhuki' => 'vidhuki/index',
    'vidhuki' => 'vidhuki/index',
    //'ru/vidhuki' => 'vidhuki/index',
//    'category/([a-z]+)' => 'catalog/category/$1', // actionCategory в CatalogController
//    'articles/([a-z]+)/([a-z]+)' => 'articles/view/$1/$2',
    'gallery/([a-z]+)' => 'photomaterial/view/$1',
     'photomaterial' => 'photomaterial/index',
    // Главная страница
    
    'index.php' => 'site/index', // actionIndex в SiteController
    '' => 'site/index', // actionIndex в SiteController
    
);
