<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/"><?=main?></a></li>
                        <li class="active"><?=vidhuki?></li>
                    </ol>
                </div>
                <h2 class="page-title"><?=vidhuki?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="vidhuk-form">
                    <div class="wrap-vidhuk-form">
                        <form method="post" id="post_form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4><?=zal_vidhuk?></h4>
                                </div>
                                <div class="col-sm-12 col-md-6"><input type="text" name="name" required class="form-control" placeholder="<?=name_fam?>:"></div>
                                <div class="col-sm-12 col-md-6"><input type="email" name="email" required class="form-control" placeholder="Email:"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12"><textarea name="message" required class="form-control" placeholder="<?=vidh?>:" style="height:90px;"></textarea></div>
                            </div>
                            <p><button type="submit" class="add-vidhuk"><?=sub_send?></button></p>
                        </form>
                        <div id="post_form_success"></div>
                    </div>
                </div>
                <?php foreach ($vidhuki as $vidhukis): ?>
                <div class="user-row-vidhuk">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h5><i class="fa fa-user" aria-hidden="true"></i>&nbsp;
                                <? echo $vidhukis['name']; ?>
                            </h5>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h6>
                                <? echo $vidhukis['date']; ?>
                            </h6>
                        </div>
                    </div>
                </div>
                <p>
                    <? echo $vidhukis['text_comment']; ?>
                </p>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php echo $pagination->get(); ?>
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/views/layouts/footer.php'; ?>