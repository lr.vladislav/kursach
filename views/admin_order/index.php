<?php include ROOT . '/views/layouts/header_admin.php'; ?>
<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Магазин</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $orderCount; ?>
                        <span>Замовлень</span></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-7">
                <h3 class="list" style="margin-bottom:20px;">Список замовлень</h3>
            </div>
            <div class="col-md-5 col-xs-5">
<!--                <a href="/admin/articles/create" class="btn-add-new-material"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Додади матеріал</a>-->
            </div>
        </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="com-md-12 ">
            <br/>
 <table class="table-bordered table-striped table">
                <tr>
                    <th>ID</th>
                    <th>Покупець</th>
                    <th>Телефон</th>
                    <th>Дата</th>
                
                    <th>Статус</th>
                    <th></th>
                 <th></th>
                </tr>
                <?php foreach ($ordersList as $order): ?>
                    <tr>
                        <td style="<?php echo Order::getStatusColor($order['status']); ?>">
                            <a href="/admin/order/view/<?php echo $order['id']; ?>">
                                <?php echo $order['id']; ?>
                            </a>
                        </td>
                        <td><?php echo $order['user_name']; ?></td>
                        <td><?php echo $order['user_phone']; ?></td>
                        <td><?php echo $order['date']; ?></td>
                   
                        <td><?php echo Order::getStatusText($order['status']); ?></td>    
                        <td style="text-align:center;"><a href="/admin/order/view/<?php echo $order['id']; ?>" title="Смотреть"><i class="fa fa-eye"></i></a></td>
                     
                        <td style="text-align:center;"><a href="/admin/order/delete/<?php echo $order['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

