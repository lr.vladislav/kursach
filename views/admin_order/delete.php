<?php include ROOT . '/views/layouts/header_admin.php'; ?>
<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Магазин</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                  
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" style="margin-top:10px;">
            <div class="col-md-7 col-xs-6">
                <h3 class="list" style="margin-bottom:20px;">Видалення замовлення</h3>
            </div>
            <div class="col-md-5 col-xs-6">
                    <form method="post">
                     <a href="/admin/order/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-del" value="Видалити" id="save">
                    </form>
            </div>
        </div>
    </div>
    <div class="back-fon">
        <div class="container">
<!--            <h4>Просмотр заказа #<?php echo $order['id']; ?></h4>-->
            <br/>




            <h5>Інформація про замовлення</h5>
            <table class="table-admin-small table-bordered table-striped table">
                <tr>
                    <td>Номер замовлення</td>
                    <td><?php echo $order['id']; ?></td>
                </tr>
                <tr>
                    <td>Покупець</td>
                    <td><?php echo $order['user_name']; ?></td>
                </tr>
                <tr>
                    <td>Телефон</td>
                    <td><?php echo $order['user_phone']; ?></td>
                </tr>
                 <tr>
                    <td>Адреса</td>
                    <td><?php echo $order['user_adress']; ?></td>
                </tr>
                <tr>
                    <td>Доставка</td>
                    <td><?php echo $order['dostavka']; ?></td>
                </tr>
                 <tr>
                    <td>Оплата</td>
                    <td><?php echo $order['many']; ?></td>
                </tr>
                <tr>
                    <td>Коментарій</td>
                    <td><?php echo $order['user_comment']; ?></td>
                </tr>
                <?php if ($order['user_id'] != 0): ?>
                    <tr>
                        <td>ID покупця</td>
                        <td><?php echo $order['user_id']; ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td><b>Статус замовлення</b></td>
                    <td>
                    <form method="post" id="post_form">
                    <input type="hidden" name="id" value="<?php echo $order['id']; ?>">
                    <div class="col-md-10">
                    <select name="status" id="target" class="form-control">
                             <option value="1" <?php if ($order['status'] == 1) echo ' selected="selected"'; ?>>Нове замовлення</option>
                             <option value="2" <?php if ($order['status'] == 2) echo ' selected="selected"'; ?>>В обробці</option>
                             <option value="3" <?php if ($order['status'] == 3) echo ' selected="selected"'; ?>>Доставляеться</option>
                             <option value="4" <?php if ($order['status'] == 4) echo ' selected="selected"'; ?>>Закрито</option>
                    </select> 
                    </div>
                     <div class="col-md-2">
                    <label for="target" style="line-height: 32px;"> <div id="post_form_success"></div></label>
                  
                      </div>
                    </form>
        </td>
                </tr>
                <tr>
                    <td><b>Дата</b></td>
                    <td><?php echo $order['date']; ?></td>
                </tr>
            </table>

            <h5>Товари в замовленні</h5>

            <table class="table-admin-medium table-bordered table-striped table ">
                <tr>
                    <th>ID товару</th>
                    <th>Артикул товару</th>
                    <th>Назва</th>
                    <th>Ціна</th>
                    <th>Кількість</th>
                </tr>
                 <?
                        $allprice=0; ?>
                <?php foreach ($products as $product): ?>
                    <tr>  
                        <td><?php echo $product['id']; ?></td>
                        <td><?php echo $product['code']; ?></td>
                        <td><?php echo $product['name']; ?></td>
                        <td><?php echo $product['price']; ?> грн</td>
                        <td><?php echo $productsQuantity[$product['id']]; ?></td>
                        <?
                        $allprice +=$product['price']*$productsQuantity[$product['id']]; ?>
                    </tr>
                 <?php endforeach; ?>
                 <tr>
                     <td colspan="4">Загальна вартість:</td>
                        <td><? echo $allprice;?> грн</td>
                 </tr>
            </table>

            <a href="/admin/order/" class="btn btn-default back"><i class="fa fa-arrow-left"></i> Назад</a>
        </div>

    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

