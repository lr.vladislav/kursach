<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/"><?=main?></a></li>
                        <li class="active">
                           <?=photomaterials?>
                        </li>
                    </ol>
                </div>
                <h1 class="page-title">
                   <?=photomaterials?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">
                <?php  if(isset($_SESSION['user'])){
                               ?>
                            <?php foreach ($galleryList as $gallery): ?>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6  text-center category">
                    <a href="/gallery/<?php echo $gallery['meta_url']; ?>?gallery=<?php echo $gallery['id']; ?>"><img src="/template/images/home/folder.png" class="category-image">  </a>
                    <a href="/gallery/<?php echo $gallery['meta_url'];?>?gallery=<?php echo $gallery['id']; ?>" class="info-me">
                        <?php echo $gallery['name']; ?>
                    </a>
                </div>
                <?php endforeach; ?>
                           <?php }   else{ ?>
                                 <img src="/template/images/doctor.png"  style="width:200px;margin-left:auto;margin-right: auto;text-align:center;display:block;">
                                 <p class="text-center denger"><?=denger?></p>
                <p class="text-center denger-message"><?=denger_message?></p>
                             <p class="text-center"><a href="/user/login"><?=denger_login?></a> | <a href="/user/register"><?=denger_register?></a></p>
                            
                              <?php   }
                     //echo $pagename; ?>
                
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/views/layouts/footer.php'; ?>
