<?php
 include ROOT . '/views/layouts/header.php';
?>
<section>
    <?php  if(isset($_SESSION['user'])){
                               ?>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/"><?=main?></a></li>
                         <li><a href="/photomaterial"><?=photomaterials; ?></a></li>
                          
                        <li class="active">
                            <? echo $pagename['name']; ?>
                        </li>
                    </ol>
                </div>
                <h1 class="page-title">
                   <? echo $pagename['name']; ?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">
            <div class="gallery-box">
            <div class="view">
                <div class="big-image"><img src="/uploads/<?=$firstimage['image'] ?>" alt="image11"></div>
                <a href="#" class="prev"></a>
                <a href="#" class="next"></a>
            </div>
         
            <div class="thumbnails">
                <div class="site-content-wrap">
<?
                foreach($photoList as $photo){
                
                ?>
        <!--gallery-->
        
                <a href="/uploads/<? echo $photo['image'];?>"><img src="/uploads/<? echo $photo['image'];?>" alt="image11"></a>
               
            <? } ?>
            </div>
        </div>
        <!--gallery end-->

            </div>
        </div>
    </div>
     </div>
</section>
 <?php }
                               ?>
<?include ROOT . '/views/layouts/footer.php'; ?>