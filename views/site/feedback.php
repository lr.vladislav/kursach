<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
       <div class="row">
              <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Главная</a></li>
                        <li class="active">Обратная связь</li>
                    </ol>
                </div>
                <h2 class="page-title">Обратная связь</h2>
            </div>
        </div>
       </div>
        <div class="row">

             <form method="post" id="post_form">
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-6"><input type="text" name="name" required class="form-control" placeholder="Имя"></div>
                                <div class="col-sm-12 col-md-6"><input type="email" name="email" required class="form-control" placeholder="Email:"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12"><textarea name="message" required class="form-control" placeholder="Текст сообщения" style="height:90px;"></textarea></div>
                            </div>
                            <p><button type="submit" class="add-vidhuk">Отправить</button></p>
                        </form> 
                        <div id="post_form_success"></div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>