<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                        <li><a href="/cart">Корзина</a></li>
                        <li class="active">
                            Оформлення замовлення
                        </li>
                    </ol>
                </div>
                <h2 class="page-title">
                    Оформлення замовлення
                </h2>
                <div class="alert alert-danger" role="alert" id="alert-checkout">
  <strong>Увага!</strong> Заповніть обов&#39;язкові поля.
</div>
           <?php if ($result): ?>
                  <div class="result-cart text-center">
                   <h2>Дякуємо!</h2>
                    <p>Ваше замовлення успішно оформлено. Ми вам зателефонуємо.</p>
                    <a href="/">Повернутися на сайт</a>
                    </div>
                    <?php else: ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="features_items">
                    <?php if (!$result): ?>
                    <div class="col-md-12">
                        <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                            <li> -
                                <?php echo $error; ?>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                        <div class="login-form">
                            <form  method="post" class="vc">
                                <h2 class="krok">1.Контактна інформація</h2>
                                <div class="form-group row">
                                    <label for="fio" class="col-sm-2 col-form-label">Ім'я, фамілія *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control  vc-fild" name="user_name" id="fio" placeholder="Ім'я, фамілія" value="<?php echo $userName; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-sm-2 col-form-label">Телефон *</label>
                                    <div class="col-sm-10">
                                        <input type="phone" class="form-control  vc-fild" name="user_phone" id="phone" placeholder="(999) 999-99-99" value="<?php echo $userPhone; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="adress" class="col-sm-2 col-form-label">Адреса *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control  vc-fild" id="user_adress" placeholder="Місто" name="user_adress" 
                                        value="<?php echo $userAdress; ?>" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="dostavka" class="col-sm-2 col-form-label">Доставка</label>
                                    <div class="col-sm-10">
                                        <select id="dostavka" class="form-control" name="dostavka">
            <option value="Нова почта">Нова почта</option>
            <option value="Інше">Інше</option>
        </select>
                                    </div>
                                </div>
                                <fieldset class="form-group row">
                                    <legend class="col-form-legend col-sm-2">Оплата</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <label class="form-check-label">
            <input class="form-check-input" type="radio" name="many" id="many1" value="Накладним платежем" checked>
            Накладним платежем
          </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
            <input class="form-check-input " type="radio" name="many" id="many2" value="Оплата онлайн">
             Оплата онлайн
          </label>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group row">
                                    <label for="dostavka" class="col-sm-2 col-form-label ">Коментар</label>
                                    <div class="col-sm-10">
                                        <textarea name="user_comment" class="form-control" placeholder="Коментар"><?php echo $userComment; ?></textarea>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <h2 class="krok">2.Підтвердження</h2>
                                <?php if ($productsInCart): ?>
                                <table class="table-bordered table-striped table">
                                    <tr>
                                    </tr>
                                    <?php foreach ($products as $product): ?>
                                    <tr>
                                        <td> <img src="<?php echo Product::getImage($product['id']); ?>" alt="" class="cart-img" /></td>

                                        <td>

                                            <?php echo $product['name'];?>

                                        </td>
                                        <td>
                                            <?php echo $productsInCart[$product['id']];?> шт</td>
                                        <td>
                                            <?php echo $product['price'];?> грн</td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td>Всього:
                                            <?php echo $totalPrice;?> грн</td>
                                    </tr>

                                </table>
                                <?php endif; ?>

                                <input type="submit" name="submit" class="inform save-order disabled" value="Оформити" />
                            </form>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>