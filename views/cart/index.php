<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
   <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                      <li class="active">Корзина</li>
                    </ol>
                </div>
                <h2 class="page-title">
                    Оформлення замовлення
                </h2>
                <div class="alert alert-danger" role="alert" id="alert-checkout">
  <strong>Увага!</strong> Заповніть обов&#39;язкові поля.
</div>
            </div>
       </div>
  <div class="row">
<div class="col-sm-9 padding-right">
                <div class="features_items">
<?php if ($productsInCart): ?>
                        <p>Ви обрали такі товари:</p>
                        <table class="table-bordered table-striped table">
                            <tr>
                                <th>Зображення</th>
                                 <th>Назва</th>
                                <th>Ціна</th>
                                <th>Кількість</th>
                                <th>Видалити</th>
                            </tr>
                            <?php foreach ($products as $product): ?>
                                <tr>
                                    <td> <img src="<?php echo Product::getImage($product['id']); ?>" alt="" class="cart-img" /></td>
                                    <td>
                                        <a href="/product/<?php echo $product['meta_url'];?>">
                                            <?php echo $product['name'];?>
                                        </a>
                                    </td>
                                    <td><?php echo $product['price'];?> грн</td>
                                    <td><?php echo $productsInCart[$product['id']];?> шт</td>
                                    <td>
                                        <a href="/cart/delete/<?php echo $product['id'];?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                <tr>
                                    <td colspan="4">Загальна вартість:</td>
                                    <td><?php echo $totalPrice;?> грн</td>
                                </tr>

                        </table>

                        <a class="btn btn-default checkout" href="/cart/checkout"><i class="fa fa-shopping-cart"></i> &nbsp;Оформити замовлення</a>
                    <?php else: ?>
                        <p>Корзина порожня </p>

                        <a class="btn btn-default checkout" href="/"><i class="fa fa-shopping-cart"></i>&nbsp;Повернутися до покупок</a>
                    <?php endif; ?>

                </div>



            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
