<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/"><?=main?></a></li>
                        <li class="active">
                            <?php echo $pagename; ?>
                        </li>
                    </ol>
                </div>
                <h1 class="page-title">
                    <?php echo $pagename; ?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">
                <?php foreach ($categoryArticles as $articles): ?>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6  text-center category">
                    <a href="/articles/<?php echo $articles['rozdil']."/".$articles['meta_url']; ?>"><img src="/template/images/home/folder.png" class="category-image">  </a>
                    <a href="/articles/<?php echo $articles['rozdil']."/".$articles['meta_url']; ?>" class="info-me">
                        <?php echo $articles['name']; ?>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/views/layouts/footer.php'; ?>
