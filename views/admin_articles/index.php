<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Матеріали</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $articlesCount; ?>
                        <span>Матеріалів</span></p>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-7">
                <h3 class="list">Список матеріалів</h3>
            </div>
            <div class="col-md-5 col-xs-5">
                <a href="/admin/articles/create" class="btn-add-new-material"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Додади матеріал</a>
            </div>
        </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="com-md-12 ">
                    <br> <br>
                    <table class="table-bordered table-striped table table-article">
                        <tr>
                            <th>Назва матеріалу</th>
                            <th>Розділ</th>
                            <th>ЧПУ (url)</th>
                            <th>Статус</th>
                            <th>Редагувати</th>
                            <th>Видалити</th>
                        </tr>
                        <?php foreach ($articlesList as $article): ?>
                        <tr>
                            <td>
                                <?php 
                            if(strlen($article['name'])>60){
                               $articlename= mb_substr($article['name'], 0, 80, 'UTF-8') . '...';
                              echo $articlename;
                            }
                            else{
                                echo $article['name'];
                            }
                            ?>
                            </td>
                            <td>
                                <?php echo $article['rozdil']; ?>
                            </td>
                            <td>
                                <?php echo $article['meta_url']; ?>
                            </td>
                            <td>
                                <?php 
                            if($article['status']=='1') 
                                echo '<a href="/articles/'.$article['rozdil'].'/'.$article['meta_url'].'/" title="Відображаеться" alt="Відображаеться"><i class="fa fa-eye active" aria-hidden="true"></i></a>';
                            else{
                                echo '<a href="/articles/'.$article['rozdil'].'/'.$article['meta_url'].'/" title="Приховано" alt="Приховано"><i class="fa fa-eye-slash disable" aria-hidden="true"></i></a>';
                                }
                            ?>
                            </td>
                            <td><a href="/admin/articles/update/<?php echo $article['id']; ?>" title="Редагувати" alt="Редагувати" class="edit"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td>
                                <a href="/admin/articles/delete/<?php echo $article['id']; ?>" title="Видалити" alt="Видалити" class="delete">
                                    <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>