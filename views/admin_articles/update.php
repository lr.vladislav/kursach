<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Матеріали</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $articlesCount; ?>

                                <span>Матеріалів</span></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Редагування матеріалу</h3>
            </div>
            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
                <div class="col-md-5 col-xs-7">
                         <a href="/admin/articles/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save"></div>
                  
                   </div>
    </div>
    <div class="back-fon">
     
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                  
                   <br>
                    <?php if (isset($errors) && is_array($errors)): ?>
                         <?php foreach ($errors as $error): ?>
                        <div class="alert alert-warning" role="alert"><?php echo $error; ?></div>
                           <?php endforeach; ?>
                      <?php endif; ?>
                    <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Назва матеріалу</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-1" placeholder="Назва матеріалу" name="name" value="<?php echo $articles['name']; ?>">
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="input-3" class="col-sm-2 control-label">Розділ</label>
                        <div class="col-sm-10">
                            <select name="rozdil" class="form-control" id="input-3">
                            <?php if (is_array($rozdilList)): ?>
                                <?php foreach ($rozdilList as $rozdil): ?>
                                    <option value="<?php echo $rozdil['url']; ?>" 
                                        <?php if ($rozdil['url'] == $articles['rozdil']) echo ' selected="selected"'; ?>>
                                        <?php echo $rozdil['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                         </select>
                        </div>
                    </div>
                   <div class="form-group">
                        <label for="editor1" class="col-sm-2 control-label">Детальний опис</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="editor1"><?php echo $articles['description']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-6" class="col-sm-2 control-label">Статус</label>
                        <div class="col-sm-10">
                            <select name="status" class="form-control" id="input-6">
                            <option value="1" selected="selected">Відображеється</option>
                            <option value="0">Приховано</option>
                        </select>
                        </div>
                    </div> 
                     <div class="form-group">
                        <label for="input-7" class="col-sm-2 control-label">ЧПУ URL матеріалу:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-7" placeholder="ЧПУ URL матеріалу" name="meta_url" value="<?php echo $articles['meta_url']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-8" class="col-sm-2 control-label">Метатег title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-8" placeholder="Метатег title" name="meta_title" value="<?php echo $articles['meta_title']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег description:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_description" id="input-9" class="form-control" placeholder="Метатег description"><?php echo $articles['meta_description']; ?></textarea>
                        </div>
                    </div>
                         <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег keywords:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_keywords" id="input-9" class="form-control" placeholder="Метатег keywords"><?php echo $articles['meta_keywords']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-66" class="col-sm-2 control-label">Індексація сторінки</label>
                        <div class="col-sm-10">
                            <select name="robots" class="form-control" id="input-66">
                            <option value="1" selected="selected">Дозволена</option>
                            <option value="0">Заборонена</option>
                        </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="input-12" class="col-sm-2 control-label">Відображаеться для:</label>
                        <div class="col-sm-10">
                            <select name="rule" id="input-12" class="form-control">
                            <option value="0" selected="selected">Всіх</option>
                          <option value="1">Зареєстрованих</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-11" class="col-sm-2 control-label">Коментарі:</label>
                        <div class="col-sm-10">
                         <select name="comment" class="form-control" id="input-11">
                            <option value="1" selected="selected">Заборонені</option>
                            <option value="0">Дозволені</option>
                        </select>
                        </div>
                    </div>
                   
                    <br><br><br><br>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>