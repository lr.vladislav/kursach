<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Фотоматеріали</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php //echo $pagesCount; ?>

                                <span>Сторінок</span></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Створення альбому</h3>
            </div>
             <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
                <div class="col-md-5 col-xs-7">
                         <a href="/admin/articles/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save"></div>
                  
                   </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   <br>
                    <?php if (isset($errors) && is_array($errors)): ?>
                         <?php foreach ($errors as $error): ?>
                        <div class="alert alert-warning" role="alert"><?php echo $error; ?></div>
                           <?php endforeach; ?>
                      <?php endif; ?>
                    <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Назва альбому</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-1" placeholder="Назва альбому" name="name" >
                        </div>
                    </div>
                  
                     <div class="form-group">
                        <label for="input-7" class="col-sm-2 control-label">ЧПУ URL альбому:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-7" placeholder="ЧПУ URL альбому" name="meta_url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-8" class="col-sm-2 control-label">Метатег title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-8" placeholder="Метатег title" name="meta_title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег description:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_description" id="input-9" class="form-control" placeholder="Метатег description"></textarea>
                        </div>
                    </div>
                         <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег keywords:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_keywords" id="input-9" class="form-control" placeholder="Метатег keywords"></textarea>
                        </div>
                    </div>
                   
                   
                    <br><br><br><br>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>