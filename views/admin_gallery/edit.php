<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Фотоматеріали</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php //echo $pagesCount; ?>

                                <span>Сторінок</span></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Редагування альбому</h3>
            </div>
         
                <div class="col-md-5 col-xs-7">
                         <a href="/admin/photomaterial/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                 </div>
                  
                   </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                  <div id="exTab1" class="container">	
<ul  class="nav nav-pills">
			<li class="active">
        <a  href="#1a" data-toggle="tab">Альбом</a>
			</li>
			<li><a href="#2a" data-toggle="tab">Фото</a>
			</li>
		
		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
          <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
 
                   <br>
                    <?php if (isset($errors) && is_array($errors)): ?>
                         <?php foreach ($errors as $error): ?>
                        <div class="alert alert-warning" role="alert"><?php echo $error; ?></div>
                           <?php endforeach; ?>
                      <?php endif; ?>
                    <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Назва альбому</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?=$gallery['name'];?>" id="input-1" placeholder="Назва альбому" name="name" >
                        </div>
                    </div>
                  
                     <div class="form-group">
                        <label for="input-7" class="col-sm-2 control-label">ЧПУ URL альбому:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?=$gallery['meta_url'];?>" id="input-7" placeholder="ЧПУ URL альбому" name="meta_url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-8" class="col-sm-2 control-label">Метатег title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?=$gallery['meta_title'];?>" id="input-8" placeholder="Метатег title" name="meta_title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег description:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_description" value="" id="input-9" class="form-control" placeholder="Метатег description"><?=$gallery['meta_description'];?></textarea>
                        </div>
                    </div>
                         <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег keywords:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_keywords"  value="" id="input-9" class="form-control" placeholder="Метатег keywords"><?=$gallery['meta_keywords'];?></textarea>
                        </div>
                    </div>
                     <div class="form-group">
                       <div class="col-sm-10">
                  
                    </div>
                     </div>
                       <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save">
                    <br><br><br><br>
               
                </form>
				</div>
				<div class="tab-pane" id="2a">
        <div class="rows">

        <? $ii=0;
            foreach($galleryobg as $obg){ $ii++; ?>
         <div class="row"><img src="/uploads/<?=$obg['image'];?>" height="80px">
         <input type="text" name="sort_order" id="" value="<?=$ii;?>">
			<input type="button" value="up">
			<input type="button" value="down">
         </div>
          <? } ?>
        </div>
         <div class="wrapper">
		<input type="file" multiple="multiple" accept=".txt,image/*">
		<a href="#" class="upload_files button">Загрузить файлы</a>
		<div class="ajax-reply"></div>
	</div>
				</div>
        
			</div>
  </div>




  
             
              <style>.wrapper{ text-align: center; margin:2em; }</style>

		

		<style>
			.button{ text-decoration: none; background:#666; padding:.3em 1em; border-radius: .3em; color:#fff; }
			.ajax-reply{ margin-top:2em; }
		</style>
              <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'></script>
	

<script>
(function($){

var files; // переменная. будет содержать данные файлов

// заполняем переменную данными файлов, при изменении значения file поля
$('input[type=file]').on('change', function(){
	files = this.files;
});


// обработка и отправка AJAX запроса при клике на кнопку upload_files
$('.upload_files').on( 'click', function( event ){

	event.stopPropagation(); // остановка всех текущих JS событий
	event.preventDefault();  // остановка дефолтного события для текущего элемента - клик для <a> тега

	// ничего не делаем если files пустой
	if( typeof files == 'undefined' ) return;

	// создадим данные файлов в подходящем для отправки формате
	var data = new FormData();
	$.each( files, function( key, value ){
		data.append( key, value );
	});

	// добавим переменную идентификатор запроса
	data.append( 'my_file_upload', 1 );

	// AJAX запрос
	$.ajax({
		url         : '/upload.php?gallery=<?=$_GET['gallery'];?>',
		type        : 'POST',
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
		contentType : false,
		// функция успешного ответа сервера
		success     : function( respond, status, jqXHR ){

			// ОК
			if( typeof respond.error === 'undefined' ){
				// файлы загружены, делаем что-нибудь

				// покажем пути к загруженным файлам в блок '.ajax-reply'

				var files_path = respond.files;
				var html = '';
				$.each( files_path, function( key, val ){
					 html += val +'<br>';
				} )

				$('.ajax-reply').html( html );
			}
			// error
			else {
				console.log('ОШИБКА: ' + respond.error );
			}
		},
		// функция ошибки ответа сервера
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});

});


})(jQuery)
</script>
                
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>