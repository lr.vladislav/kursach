<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row" >
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Фотоматеріали</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
<!--
                    <ul class="shape">
                        <li>
                            <p>
                                <?php //echo $pagesCount; ?>

                                <span>Сторінок</span></p>
                        </li>
                        
                    </ul>
-->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row"  style="margin-top:10px;">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Список альбомів</h3>
            </div>
            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
                <div class="col-md-5 col-xs-7">
                    <a href="/admin/photomaterial/create" class="btn-add-new"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Альбом</a>
<!--
                         <a href="/admin/pages/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save">
-->
                </div>
                  
                   </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="com-md-12 ">
                    <br> <br>
                    <table class="table-bordered table-striped table table-article">
                        <tr>
                             <th>Фото</th>
                            <th>Назва альбому</th>
                            <th>ЧПУ (url)</th>
                          
                            <th>Редагувати</th>
                            <th>Видалити</th>
                        </tr>
                        <?php foreach ($galleryList as $gallery): ?>
                        <tr>
                            <td></td>
                            <td>
                                <?php 
//                            if(strlen($pages['name'])>60){
//                               $pagename= mb_substr($pages['name'], 0, 80, 'UTF-8') . '...';
//                              echo $pageename;
//                            }
//                            else{
//                                echo $pages['name'];
//                            }
                              echo $gallery['name'];
                            ?>
                            </td>
                            <td  style="width:auto;">
                                <?php echo $gallery['meta_url']; ?>
                            </td>
                          
                            <td><a href="/admin/photomaterial/update/<?php echo $gallery['id']; ?>?gallery=<?php echo $gallery['id']; ?>" title="Редагувати" alt="Редагувати" class="edit"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td>
                                <a href="/admin/pages/delete/<?php echo $gallery['id']; ?>" title="Видалити" alt="Видалити" class="delete">
                                    <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>