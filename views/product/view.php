<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                        <li class="active">
                            <?php echo $product['name']; ?>
                        </li>
                    </ol>
                </div>
                <h1 class="page-title">
                    <?php echo $product['name']; ?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">
                <div class="product-details">
                    <!--product-details-->
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="view-product">
                                <img src="<?php echo Product::getImage($product['id']); ?>" alt="" />
                              
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="product-information">
                                <!--/product-information-->


                                <h4>Купити
                                    <?php echo $product['name']; ?>
                                </h4>
                                <p>Код товару:
                                    <?php echo $product['code']; ?>
                                </p>
                                <p class="yes"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;В наявності</p>
                                <p>Переглядів:150</p>
                                <p class="price">
                                    <?php echo $product['price']; ?> грн</p>
                                <a href="#" data-id="<?php echo $product['id']; ?>" class="btn add-to-cart">
                                    <i class="fa fa-shopping-cart"></i>Додати в корзину
                                </a>


                            </div>
                            <!--/product-information-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <br/>
                            <h5 class="view-descrip">Опис товару</h5>
                            <?php echo $product['description']; ?>
                        </div>
                       
                    </div>
                </div>
                <!--/product-details-->

            </div>
        </div>
    </div>

</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>