<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Сторінки</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $pagesCount; ?>
                        <span>Сторінок</span></p>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-7">
                <h3 class="list">Список сторінок</h3>
            </div>
            <div class="col-md-5 col-xs-5">
                <a href="/admin/pages/create" class="btn-add-new-material"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Додади сторінку</a>
            </div>
        </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="com-md-12 ">
                    <br> <br>
                    <table class="table-bordered table-striped table table-article">
                        <tr>
                            <th>Назва сторінки</th>
                            <th>ЧПУ (url)</th>
                            <th>Статус</th>
                            <th>Редагувати</th>
                            <th>Видалити</th>
                        </tr>
                        <?php foreach ($pagesList as $pages): ?>
                        <tr>
                            <td>
                                <?php 
//                            if(strlen($pages['name'])>60){
//                               $pagename= mb_substr($pages['name'], 0, 80, 'UTF-8') . '...';
//                              echo $pageename;
//                            }
//                            else{
//                                echo $pages['name'];
//                            }
                              echo $pages['name'];
                            ?>
                            </td>
                            <td  style="width:auto;">
                                <?php echo $pages['meta_url']; ?>
                            </td>
                            <td style="width:100px;text-align:center;">
                                <?php 
                            if($pages['status']=='1') 
                                echo '<a href="/content/'.$pages['meta_url'].'/" title="Відображаеться" alt="Відображаеться"><i class="fa fa-eye active" aria-hidden="true"></i></a>';
                            else{
                                echo '<a href="/content/'.$pages['meta_url'].'/" title="Приховано" alt="Приховано"><i class="fa fa-eye-slash disable" aria-hidden="true"></i></a>';
                                }
                            ?>
                            </td>
                            <td><a href="/admin/pages/update/<?php echo $pages['id']; ?>" title="Редагувати" alt="Редагувати" class="edit"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td>
                                <a href="/admin/pages/delete/<?php echo $pages['id']; ?>" title="Видалити" alt="Видалити" class="delete">
                                    <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>