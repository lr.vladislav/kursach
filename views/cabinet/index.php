<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                        <li class="active">
                          Кабінет користувача
                        </li>
                    </ol>
                </div>
                <h2 class="page-title">
                  Кабінет користувача
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">


            <h4>Доброго дня, <?php echo $user['name'];?>!</h4>
            <?php if($user['role']=='admin'):?>
                <a href="/admin"><i class="fa fa-spinner" aria-hidden="true"></i>&nbsp;Адмінпанель</a><br>
                <? endif;?>
          <a href="/cabinet/edit"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Редагувати особисті дані</a>
        

        </div>
    </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
