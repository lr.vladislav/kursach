<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                        <li><a href="/cabinet">Кабінет користувача</a></li>
                        <li class="active">Редагування особистих данних  </li>
                    </ol>
                </div>
                <h2 class="page-title">
                Редагування особистих данних
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">

                <?php if ($result): ?>
                  <div class="alert alert-success">
  <strong>Увага!</strong> Дані успішно збережені.
</div>
                <?php else: ?>
                    <?php if (isset($errors) && is_array($errors)): ?>

                            <?php foreach ($errors as $error): ?>
                              <div class="alert alert-danger">
  <strong>Увага!</strong> <?php echo $error; ?>
</div>
          <?php endforeach; ?>

                    <?php endif; ?>

                    <form action="#" method="post" class="form-horizontal" >
                      <div class="form-group ">
                          <label for="input-2" class="col-sm-1 control-label label-left">Логін:</label>
                          <div class="col-sm-10 col-md-4">
                              <input type="text" class="form-control" name="name" placeholder="Логін:" value="<?php echo $email; ?>" disabled />
                        </div>
                      </div>
                      <div class="form-group ">
                          <label for="input-2" class="col-sm-1 control-label label-left">Ім&#39;я:</label>
                          <div class="col-sm-10 col-md-4">
                              <input type="text" class="form-control" name="name" placeholder="Ім&#39;я:" value="<?php echo $name; ?>"/>
                        </div>
                      </div>
                      <div class="form-group">
                          <label for="input-2" class="col-sm-1 control-label label-left">Пароль:</label>
                          <div class="col-sm-10 col-md-4">
                              <input type="password" name="password" class="form-control" placeholder="Пароль" value="<?php echo $password; ?>"/>
                    </div>
                      </div>
                                              <input type="submit" name="submit" class="btn btn-default" value="Зберегти" />
                    </form>

                    </div><!--/sign up form-->

                <?php endif; ?>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
