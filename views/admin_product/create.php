<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Магазин</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $productsCount; ?>

                                <span>Товари</span></p>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Додавання нового товару</h3>
            </div>
            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
                <div class="col-md-5 col-xs-7">
                      <a href="/admin/product/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save"></div>
                  
                   </div>
    </div>
    
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="exTab2" class="container">	
<ul class="nav nav-tabs">
			<li class="active">
        <a href="#1" data-toggle="tab">Українська</a>
			</li>
			<li><a href="#2" data-toggle="tab">Русский</a>
			</li>
			
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="1">
          <h3>Standard tab panel created on bootstrap using nav-tabs</h3>
				</div>
				<div class="tab-pane" id="2">
          <h3>Notice the gap between the content and tab after applying a background color</h3>
				</div>
</div>
                   <br>
                    <?php if (isset($errors) && is_array($errors)): ?>
                         <?php foreach ($errors as $error): ?>
                        <div class="alert alert-warning" role="alert"><?php echo $error; ?></div>
                           <?php endforeach; ?>
                      <?php endif; ?>
                    <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Назва товару</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-1" placeholder="Назва товару" name="name" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-2" class="col-sm-2 control-label">Артикул</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-2" placeholder="Артикул" name="code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-3" class="col-sm-2 control-label">Ціна, грн</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-3" placeholder="Ціна" name="price">
                        </div>
                    </div>
                   <div class="form-group">
                        <label for="input-5" class="col-sm-2 control-label">Зображення товару</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="input-5" placeholder="Ціна" name="image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editor1" class="col-sm-2 control-label">Детальний опис</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="editor1"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-6" class="col-sm-2 control-label">Статус</label>
                        <div class="col-sm-10">
                            <select name="status" class="form-control" id="input-6">
                            <option value="1" selected="selected">Відображеється</option>
                            <option value="0">Приховано</option>
                        </select>
                        </div>
                    </div> 
                       <div class="form-group">
                        <label for="input-12" class="col-sm-2 control-label">Рекумендуємі</label>
                        <div class="col-sm-10">
                            <select name="is_recommended" class="form-control" id="input-12">
                            <option value="0" selected="selected">ні</option>
                            <option value="1">так</option>
                        </select>
                        </div>
                    </div>                    <div class="form-group">
                        <label for="input-7" class="col-sm-2 control-label">ЧПУ URL матеріалу:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-7" placeholder="ЧПУ URL матеріалу" name="meta_url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-8" class="col-sm-2 control-label">Метатег title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-8" placeholder="Метатег title:" name="meta_title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег description:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_description" id="input-9" class="form-control"></textarea>
                        </div>
                    </div>
                         <div class="form-group">
                        <label for="input-9" class="col-sm-2 control-label">Метатег keywords:</label>
                        <div class="col-sm-10">
                            <textarea name="meta_keywords" id="input-9" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-12" class="col-sm-2 control-label">Відображаеться для:</label>
                        <div class="col-sm-10">
                            <select name="user_use" id="input-12" class="form-control">
                            <option value="0" selected="selected">Всіх</option>
                          <option value="1">Зареєстрованих</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-11" class="col-sm-2 control-label">Коментарі:</label>
                        <div class="col-sm-10">
                         <select name="comment" class="form-control" id="input-11">
                            <option value="0" selected="selected">Заборонені</option>
                            <option value="1">Дозволені</option>
                        </select>
                        </div>
                    </div>
                   
                    <br><br><br><br>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>