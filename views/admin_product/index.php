<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Магазин</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $productsCount; ?>

                                <span>Товари</span></p>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-7">
                <h3 class="list">Список товарів</h3>
            </div>
            <div class="col-md-5 col-xs-5">
                <a href="/admin/products/create" class="btn-add-new"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Додади товар</a>
            </div>
        </div>
    </div>
        <div class="back-fon">
        <div class="container">
        <div class="row">
            <div class="com-md-12 ">
               <br>  <br>
                <table class="table-bordered table-striped table table-product">
                    <tr>
                        <!--                    <th>ID товару</th>-->
                        <th>Зображення</th>
                        <th>Артикул</th>
                        <th>Назва товару</th>
                        <th>Ціна</th>
                         <th>Статус</th>
                        <th>Редагувати</th>
                        <th>Видалити</th>
                    </tr>
                    <?php foreach ($productsList as $product): ?>
                    <tr>
                        <td>
                             <img src="<?php echo Product::getImage($product['id']); ?>" alt="" class="img-priew"/>
                          </td>
                        <td>
                            <?php echo $product['code']; ?>
                        </td>
                        <td>
                            <?php echo $product['name']; ?>
                        </td>
                        <td>
                            <?php echo $product['price']; ?> грн
                        </td>
                        <td>
                        <?php 
                            if($product['status']=='1') 
                                echo '<a href="/product/'.$product["meta_url"].'" title="Відображаеться" alt="Відображаеться"><i class="fa fa-eye active" aria-hidden="true"></i></a>';
                            else{
                                echo '<a href="/product/'.$product["meta_url"].'" title="Приховано" alt="Приховано"><i class="fa fa-eye-slash disable" aria-hidden="true"></i></a>';
                                }
                            ?>
                         </td>
                        <td><a href="/admin/products/update/<?php echo $product['id']; ?>" title="Редагувати" alt="Редагувати" class="edit"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td>
                            <a href="/admin/products/delete/<?php echo $product['id']; ?>" title="Видалити" alt="Видалити" class="delete">
<!--                                <i class="fa fa-times"></i>-->
                           <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;"></i>
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>
    </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>