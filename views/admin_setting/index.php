<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Сайт</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                  
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row"  style="margin-top:10px;">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Налаштування</h3>
            </div>
            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
                <div class="col-md-5 col-xs-7">

                    <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save"></div>

             </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Назва сайту (title)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-1" placeholder="Назва сайту (title)" name="meta_title" value="<?php echo $setting['meta_title']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-2" class="col-sm-2 control-label">Опис (Description)</label>
                        <div class="col-sm-10">
                                  <textarea class="form-control" id="input-2" placeholder="Опис (Description)" name="meta_description" ><?php echo $setting['meta_description']; ?></textarea>
                              </div>
                    </div>
                    <div class="form-group">
                        <label for="input-3" class="col-sm-2 control-label">Ключові слова (Keywords)</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="input-3" placeholder="Ключові слова (Keywords)" name="meta_keywords"  /><?php echo $setting['meta_keywords']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-4" class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="input-4" placeholder="Email:" name="email" value="<?php echo $setting['email']; ?>">
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="input-5" class="col-sm-2 control-label">Вимкнути сайт:</label>
                        <div class="col-sm-10">
                          <select class="form-control" name="site_status" id="input-5">
                              <option valuse="0">ні</option>
                            <option valuse="1">Так</option>
                          </select>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="input-6" class="col-sm-2 control-label">Причина вимкнення сайту</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="input-6" placeholder="Причина вимкнення сайту" name="status_test"  /><?php echo $setting['status_test']; ?></textarea>
                        </div>
                    </div>
                  <br><br>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>
