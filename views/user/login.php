<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/">Головна</a></li>
                        <li class="active">
                          Вхід на сайт
                        </li>
                    </ol>
                </div>
                <h2 class="page-title">
                  Вхід на сайт
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">
                <div class="product-details">
                  <?php if (isset($errors) && is_array($errors)): ?>
                  <?php foreach ($errors as $error): ?>
                    <div class="alert alert-danger">
                  <strong>Помилка!</strong> <?php echo $error; ?>
                  </div>
                      <?php endforeach; ?>
                      <?php endif; ?>


                    <form action="#" method="post" class="form-horizontal" >
                      <div class="form-group ">
                          <label for="input-2" class="col-sm-1 control-label label-left">Логін:</label>
                          <div class="col-sm-10 col-md-4">
                              <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                        </div>
                      </div>
                      <div class="form-group">
                          <label for="input-2" class="col-sm-1 control-label label-left">Пароль:</label>
                          <div class="col-sm-10 col-md-4">
                              <input type="password" name="password" class="form-control" placeholder="Пароль" value="<?php echo $password; ?>"/>
                    </div>
                      </div>
                                              <input type="submit" name="submit" class="btn btn-default" value="Вхід" /><br/><br/>
                                              <a href="/user/register">Реєстрація на сайті</a>
                    </form>
                </div><!--/sign up form-->



            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
