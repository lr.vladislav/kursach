<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Відгуки</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $vidhukCount; ?>

                                <span>Відгуків</span></p>
                        </li>
                      
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Редагування відгуку</h3>
            </div>
            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id='form1' name='form1'>
                <div class="col-md-5 col-xs-7">
                         <a href="/admin/vidhuk/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-save" value="Зберегти" id="save"></div>
                  
                   </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   <br>
                    <?php if (isset($errors) && is_array($errors)): ?>
                         <?php foreach ($errors as $error): ?>
                        <div class="alert alert-warning" role="alert"><?php echo $error; ?></div>
                           <?php endforeach; ?>
                      <?php endif; ?>
                    <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Ім'я</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-1" placeholder="Ім'я" name="name" value="<?php echo $vidhuk['name']; ?>">
                        </div>
                    </div>
                       <div class="form-group">
                        <label for="input-2" class="col-sm-2 control-label">Дата</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-2" placeholder="Дата" name="date" value="<?php echo $vidhuk['date']; ?>">
                        </div>
                    </div>
                   <div class="form-group">
                        <label for="editor1" class="col-sm-2 control-label">Відгук</label>
                        <div class="col-sm-10">
                            <textarea name="text_comment" id="editor1"><?php echo $vidhuk['text_comment']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-6" class="col-sm-2 control-label">Статус</label>
                        <div class="col-sm-10">
                            <select name="status" class="form-control" id="input-6">
                            <option value="1" selected="selected">Відображеється</option>
                            <option value="0">Приховано</option>
                        </select>
                        </div>
                    </div> 
                     
                   
                    <br><br><br><br>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>