<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Відгуки</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $vidhukCount; ?>

                                <span>Відгуків</span></p>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-5">
                <h3 class="list">Видалення відгуку</h3>
            </div>
            <form  method="post">
                <div class="col-md-5 col-xs-7">
                         <a href="/admin/vidhuk/" class="btn-add-left"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Назад</a>
                    <input type="submit" name="submit" class="btn-add-del" value="Видалити" id="save"></div>
                  
                   </div>

                </form>
            </div>
            <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                 <div class="form-group">
                        <label for="input-1" class="col-sm-2 control-label">Ім'я</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-1" placeholder="Назва сторінки" name="name" value="<?php echo $vidhuk['name']; ?>" disabled >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editor1" class="col-sm-2 control-label">Відгук</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="editor1" disabled ><?php echo $vidhuk['text_comment']; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
                </div>
        </div>
 </section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>