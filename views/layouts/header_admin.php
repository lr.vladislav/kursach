<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Админпанель</title>
    <link href="/template/css/bootstrap.min.css" rel="stylesheet">
    <link href="/template/css/font-awesome.min.css" rel="stylesheet">
    <link href="/template/css/prettyPhoto.css" rel="stylesheet">
    <link href="/template/css/price-range.css" rel="stylesheet">
    <link href="/template/css/animate.css" rel="stylesheet">
    <link href="/template/css/main.css" rel="stylesheet">
    <link href="/template/css/responsive.css" rel="stylesheet">
    <link href="/template/css/sweet-alert.css" rel="stylesheet">
      <link href="/template/css/vue-tub.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    <link rel="shortcut icon" href="/template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/template/images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
    <div class="page-wrapper">

        <header id="header">
            <div class="header_top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
                                        <a class="navbar-brand logo" href="/admin/">MacleStudio</a>
                                    </div>
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                        </ul>
                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="dropdown">
                                                <a href="/admin/product" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="/cabinet/"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Кабінет</a></li><li><a href="/user/logout/"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Вихід</a></li>

                                                </ul>
                                            </li>
                                             <li class="dropdown">
                                                <a href="/admin/product" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"  style="
                                                    <? if($orderAlert > 0 ){echo 'color:#de5d5d;';} ?>
                                                "></i><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="/admin/order"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;
                                                            (<? echo $orderAlert; ?>) нових замовлень</a>
                                                    </li>

                                                </ul>
                                            </li>
   <!--        <li><a href="/admin/"><i class="fa fa-user" aria-hidden="true"></i></a></li>-->
                                            <li><a href="/admin/">Головна</a></li>
                                            <li class="dropdown">
                                                <a href="/admin/product" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Зміст <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="/admin/vidhuk">Відгуки</a></li>
                                                    <li><a href="/admin/product">Магазин</a></li>
                                                    <li><a href="/admin/pages">Сторінки</a></li>
                                                    <li><a href="/admin/articles">Матеріали</a></li>
                                                    <li><a href="/admin/ask">Запитання</a></li>
                                                    <li><a href="/admin/photomaterial">Фотоматеріали</a></li>
                                                    <li><a href="/admin/order">Список замовлень</a></li>

                                                </ul>
                                            </li>
                                            <li><a href="/admin/setting">Налаштування</a></li>
                                            <li><a href="/">На сайт</a></li>

                                            <!--<li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i></a></li>-->
                                            <!--
          <form class="navbar-form navbar-left">
        <div class="form-groupp">
          <input type="text" class="field-search" placeholder="Пошук">
        </div>

      </form>
-->
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
            <!--/header_top-->
