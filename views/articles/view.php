<?php
 include ROOT . '/views/layouts/header.php';
?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/"><?=main?></a></li>
                         <li><a href="<? echo $pagelink; ?>"><?php echo $pagename; ?></a></li>
                        <li class="active">
                            <? echo $articles['name']; ?>
                        </li>
                    </ol>
                </div>
                <h1 class="page-title">
                   <? echo $articles['name']; ?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 padding-right">
<? echo $articles['description']; ?>
            </div>
        </div>
    </div>
</section>
<?include ROOT . '/views/layouts/footer.php'; ?>