<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Запитання</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                    <ul class="shape">
                        <li>
                            <p>
                                <?php echo $askCount; ?>
                        <span>Запитань</span></p>
                        </li>
  </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-7" style="margin-bottom:10px;">
                <h3 class="list">Список запитань</h3>
            </div>

        </div>
    </div>
    <div class="back-fon">
        <div class="container">
            <div class="row">
                <div class="com-md-12 ">
                    <br> <br>
                    <table class="table-bordered table-striped table ">
                        <tr>
                            <th>id</th>
                            <th>Дата</th>
                            <th>Ім'я</th>
                            <th style="text-align:center;">Статус</th>
                            <th style="text-align:center;">Редагувати</th>
                            <th style="text-align:center;">Видалити</th>
                        </tr>

                        <?php foreach ($vidhukiListt as $vidhuks): ?>
                        <tr>
                            <td>
                                <?php

                                echo $vidhuks['id'];

                            ?>
                            </td>
                            <td>
                                <?php echo $vidhuks['date']; ?>
                            </td>
                            <td>
                                <?php echo $vidhuks['name']; ?>
                            </td>
                            <td style="text-align:center;">
                                <?php
                            if($vidhuks['status']=='1')
                                echo '<a><i class="fa fa-eye active" aria-hidden="true"></i></a>';
                            else{
                                echo '<a><i class="fa fa-eye-slash disable" aria-hidden="true"></i></a>';
                                }
                            ?>
                            </td>
                            <td style="text-align:center;"><a href="/admin/vidhuk/update/<?php echo $vidhuks['id'];?>" title="Редагувати" alt="Редагувати" class="edit"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td style="text-align:center;">
                                <a href="/admin/vidhuk/delete/<?php echo $vidhuks['id'];?>" title="Видалити" alt="Видалити" class="delete">
                                    <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>
