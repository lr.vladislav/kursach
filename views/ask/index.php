<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/"><?=main?></a></li>
                        <li class="active">Задати питання</li>
                    </ol>
                </div>
                <h2 class="page-title">Задати питання</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 ">
                <div class="vidhuk-form">
                    <div class="wrap-vidhuk-form">
                        <form method="post" id="post_form_ask">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <h4>Задати питання</h4>
                                </div>
                                <div class="col-sm-6 col-md-12">
                                  <input type="text" name="name" required class="form-control" placeholder="Ім'я, фамілія:" >
                              </div>
                                </div>
                                  <div class="row">
                                <div class="col-sm-6 col-md-12"><input type="email" name="email" required class="form-control" placeholder="Email:" aria-describedby="emailHelp">
                                <small id="emailHelp" class="form-text text-muted">Вкажіть email на який прийде відповідь на ваше питання.</small></div>
                            </div>
                            <div class="row">
                          <div class="col-sm-6 col-md-12"><input type="tel" name="phone" id="phone" required class="form-control" placeholder="Телефон в форматі (099) 999-99-99" >
                            </div>
                      </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-12"><textarea name="message" required class="form-control" placeholder="Запитання:" style="height:90px;"></textarea></div>
                            </div>
                            <p><button type="submit" class="add-vidhuk">Відправити</button></p>
                        </form>
                        <div id="post_form_success"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<?php include ROOT . '/views/layouts/footer.php'; ?>
