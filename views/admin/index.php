<?php include ROOT . '/views/layouts/header_admin.php'; ?>
<section>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-3 col-xs-3">
                    <h2 class="page-name">Головна</h2>
                </div>
                <div class="col-md-5 col-sm-9 col-xs-9">
                  
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" style="margin-top:10px;">
            <div class="col-md-7 col-xs-6">
            <br><br>
<!--                <h3 class="list" style="margin-bottom:20px;">Перегляд замовлень</h3>-->
            </div>
            <div class="col-md-5 col-xs-6">
<!--                <a href="/admin/order" class="btn-add-new-material-2"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;Список замовлень</a>-->
            </div>
        </div>
    </div>
<!--
    <div class="back-fon">
        <div class="container">
-->

<style>
    body{
/*        background-color: #d5d5d5;*/
/*
        background: url('/template/images/admin/admin.jpg');
        background-size: cover;
*/
    }
    #footer{
        background-color: rgba(0, 0, 0, 0) !important;
    }
</style>

    <div class="container">
       
        <div class="row">
          <div class="col-md-6">
          <p class="all-stats">Статистика магазину за весь час</p>
           <div class=" pull-left stats-block">
               <p>Всього продано на сумму:
               <? 
                   $mas=array();
               foreach($orderStats as $statistic)
               {
                $productsQuantity = json_decode($statistic['products'], true);
                $productsIds = array_keys($productsQuantity);
                $products = Product::getProdustsByIds($productsIds);
                $allprice=0;
                $count=0;
                foreach($products as $product){
                    //echo   $product['name'];
                    $allprice +=$product['price']*$productsQuantity[$product['id']];
                }
                     $mas[]=$allprice;
                   //echo $allprice." грн ";
                }
                echo "<span>".array_sum($mas) ." грн</span>";
               ?>
               </p>
               <p>Всього замовлень: <span><?=$orderStatsCount ?></span></p>
               <p>Нових замовлень: <span><?=$orderAlert?></span></p>
           </div>
            </div>
              <div class="col-md-6">
          <p class="all-stats">Останні 10 замовлень</p>
           <div class="pull-right stats-block">
                 <table class="table-bordered table-striped table">
                <tr>
                    <th>ID</th>
                    <th>Покупець</th>
                 
                    <th>Дата</th>
                
                  
                   
                 <th></th>
                </tr>
                 <?
               foreach($lastorder as $last){ ?>
                   <tr>
                        <td style="<?php echo Order::getStatusColor($last['status']); ?>">
                            <a href="/admin/order/view/<?php echo $order['id']; ?>">
                                <?php echo $last['id']; ?>
                            </a>
                        </td>
                        <td><?php echo $last['user_name']; ?></td>
                      
                        <td><?php echo $last['date']; ?></td>
                   
                       
                        <td style="text-align:center;"><a href="/admin/order/view/<?php echo $last['id']; ?>" title="Смотреть"><i class="fa fa-eye"></i></a></td>
                     
                     
                    </tr>
              <? }
               ?>
                 </table>
                  <a href="/admin/order" style="margin-left:auto;margin-right:auto;float:none;" class="btn-add-new-material-2"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;Список замовлень</a>
                  </div>
            </div>
<!--
           <div class="col-md-12 text-center">
               
                  <h1 class="" style="color:#605f5f;">Сторінка ще недороблена!</h1>
                  <br/>   <br/>
              <a href="/admin/order" style="margin-left:auto;margin-right:auto;float:none;" class="btn-add-new-material-2"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;Список замовлень</a>
            </div>
-->
             
        </div>
          <br> <br> <br>
    </div>
    
</section>

<?php  include ROOT . '/views/layouts/footer_admin.php'; ?>

