<?php

class Ask
    {


     public static function getAllAsk()
    {
        $db = Db::getConnection();
        $sql = 'SELECT id, date, name, text_comment, status FROM vidhuki '
                . 'ORDER BY id DESC';
        $result = $db->prepare($sql);
        $result->execute();
         $i = 0;
        $vidhukiListt = array();
        while ($row = $result->fetch()) {
            $vidhukiListt[$i]['id'] = $row['id'];
            $vidhukiListt[$i]['date'] = $row['date'];
            $vidhukiListt[$i]['name'] = $row['name'];
            $vidhukiListt[$i]['text_comment'] = $row['text_comment'];
            $vidhukiListt[$i]['status'] = $row['status'];
            $i++;
        }
        return $vidhukiListt;
    }
     public static function getAskCount()
    {
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM asks');
         $row = $result->fetch();
         $askCount=$row[0];
         return  $askCount;
    }
     public static function getAskAdminById($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM vidhuki WHERE id=:id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
     public static function updateAskById($id, $options)
    {
        $db = Db::getConnection();
        $sql = "UPDATE vidhuki
            SET
                name = :name,
                date = :date,
                text_comment = :text_comment,
                status = :status
           WHERE id= :id";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':date', $options['date'], PDO::PARAM_STR);
        $result->bindParam(':text_comment',  $options['text_comment'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
     public static function deleteAskById($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM vidhuki WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

}
