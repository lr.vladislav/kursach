<?php


class Order
{
    public static function save($userName, $userPhone, $userAdress, $userHome, $userMany, $userComment, $userId, $products)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO product_order (user_name, user_phone, user_adress, dostavka, many, user_comment, user_id, products) '
                . 'VALUES (:user_name, :user_phone, :user_adress, :dostavka, :many, :user_comment, :user_id, :products)';
        $products = json_encode($products);
        $result = $db->prepare($sql);
        $result->bindParam(':user_name', $userName, PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, PDO::PARAM_STR);
        $result->bindParam(':user_adress', $userAdress, PDO::PARAM_STR);
        $result->bindParam(':dostavka', $userHome, PDO::PARAM_STR);
        $result->bindParam(':many', $userMany, PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, PDO::PARAM_STR);
        $result->bindParam(':user_id', $userId, PDO::PARAM_STR);
        $result->bindParam(':products', $products, PDO::PARAM_STR);
        return $result->execute();
    }
     public static function saves($userName, $userPhone, $userAdress, $userHome, $userMany, $userComment, $userId, $products)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO product_order_stats (user_name, user_phone, user_adress, dostavka, many, user_comment, user_id, products) '
                . 'VALUES (:user_name, :user_phone, :user_adress, :dostavka, :many, :user_comment, :user_id, :products)';
        $products = json_encode($products);
        $result = $db->prepare($sql);
        $result->bindParam(':user_name', $userName, PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, PDO::PARAM_STR);
        $result->bindParam(':user_adress', $userAdress, PDO::PARAM_STR);
        $result->bindParam(':dostavka', $userHome, PDO::PARAM_STR);
        $result->bindParam(':many', $userMany, PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, PDO::PARAM_STR);
        $result->bindParam(':user_id', $userId, PDO::PARAM_STR);
        $result->bindParam(':products', $products, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function getOrdersList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, user_name, user_phone, date, status FROM product_order ORDER BY id DESC');
        $ordersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_phone'] = $row['user_phone'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['status'] = $row['status'];
//             $ordersList[$i]['meta_url'] = $row['meta_url'];
            $i++;
        }
        return $ordersList;
    }
     public static function getOrdersListMain()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, user_name, user_phone, date, status FROM product_order ORDER BY id DESC LIMIT 10');
        $ordersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_phone'] = $row['user_phone'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['status'] = $row['status'];
//             $ordersList[$i]['meta_url'] = $row['meta_url'];
            $i++;
        }
        return $ordersList;
    }

    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
              return 'Нове замовлення';
              break;
            case '2':
                return 'В обробці';
                break;
            case '3':
                return 'Доставляеться';
                break;
            case '4':
                return 'Закритий';
                break;
        }
    }
    public static function getStatusColor($status)
    {
        switch ($status) {
            case '1':
              return 'border-left: 3px solid #4ce839;';
              break;
            case '2':
                return 'В обробці';
                break;
            case '3':
                return 'Доставляеться';
                break;
            case '4':
                return 'Закритий';
                break;
        }
    }

 public static function getOrderById($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM product_order WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }

    public static function deleteOrderById($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM product_order WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateOrderById($id, $userName, $userPhone, $userComment, $date, $status)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE product_order
            SET
                user_name = :user_name,
                user_phone = :user_phone,
                user_comment = :user_comment,
                date = :date,
                status = :status
            WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':user_name', $userName, PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, PDO::PARAM_STR);
        $result->bindParam(':date', $date, PDO::PARAM_STR);
        $result->bindParam(':status', $status, PDO::PARAM_INT);
        return $result->execute();
    }
     public static function getOrderCount()
    {
         // Соединение с БД
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM product_order');
         $row = $result->fetch();
         $orderCount=$row[0];
         return  $orderCount;
    }
      public static function getOrderAlert()
    {
         // Соединение с БД
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM product_order WHERE status =1');
         $row = $result->fetch();
         $orderAlert=$row[0];
         return  $orderAlert;
    }
     public static function getOrderStats()
    {
         // Соединение с БД
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM product_order_stats');
         $row = $result->fetch();
         $orderStatsCount=$row[0];
         return  $orderStatsCount;
    }
    public static function getOrdersListStats()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, date, status, products FROM product_order_stats ORDER BY id DESC');
        $ordersListStats = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersListStats[$i]['id'] = $row['id'];
            $ordersListStats[$i]['date'] = $row['date'];
            $ordersListStats[$i]['status'] = $row['status'];
            $ordersListStats[$i]['products'] = $row['products'];
            $ordersListStats[$i]['status'] = $row['status'];
            $i++;
        }
        return $ordersListStats;
    }

}
