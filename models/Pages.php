<?php

class Pages
{
   
    public static function getPagesById($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM pages WHERE meta_url LIKE  :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
    public static function getPagesAdminById($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM pages WHERE id=:id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
    
    public static function getPagesList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, name,  status, meta_url FROM pages ORDER BY id ASC');
        $pagesList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $pagesList[$i]['id'] = $row['id'];
            $pagesList[$i]['name'] = $row['name'];
            $pagesList[$i]['status'] = $row['status'];
            $pagesList[$i]['meta_url'] = $row['meta_url'];
          
            $i++;
        }
        return $pagesList;
    }
   
    public static function deletePagesById($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM pages WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    
    public static function updatePagesById($id, $options)
    {
        $db = Db::getConnection();
        $sql = "UPDATE pages
            SET 
                user_id = :user_id, 
                name = :name, 
                date = :date, 
                description = :description, 
                status = :status,
                is_comment = :comment,
                is_robots = :robots,
                meta_url = :meta_url,
                meta_title = :meta_title, 
                meta_description = :meta_description, 
                meta_keywords = :meta_keywords,
                rulee = :rulee
            WHERE id= :id";
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $options['user_id'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':date',  $options['date'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':comment', $options['comment'], PDO::PARAM_INT);
        $result->bindParam(':robots', $options['robots'], PDO::PARAM_STR);
        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
        $result->bindParam(':rulee', $options['rulee'], PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    
    public static function createPages($options)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO pages '
                . '(user_id, name, date, description, status, is_comment, is_robots, meta_url, meta_title, meta_description, meta_keywords,'
                . 'rulee)'
                . 'VALUES '
                . '(:user_id, :name, :date, :description, :status, :comment,'
                . ':robots, :meta_url, :meta_title, :meta_description, :meta_keywords, :rulee)';
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $options['user_id'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':date',  $options['date'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':comment', $options['comment'], PDO::PARAM_INT);
        $result->bindParam(':robots', $options['robots'], PDO::PARAM_STR);
        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
        $result->bindParam(':rulee', $options['rulee'], PDO::PARAM_STR);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
            return 0;
    }
    
    public static function getPagesCount()
    {
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM pages');
         $row = $result->fetch();
         $pagesCount=$row[0];
         return  $pagesCount;
    }
    
}