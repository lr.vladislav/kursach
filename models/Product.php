<?php


class Product
{
    const SHOW_BY_DEFAULT = 6;
    public static function getLatestProducts($count = self::SHOW_BY_DEFAULT)
    {
        $db = Db::getConnection();
        $sql='SELECT id,  name, price, code, status, meta_url FROM product WHERE status = "1" LIMIT 3';
        $result = $db->prepare($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['meta_url'] = $row['meta_url'];
            $i++;
        }
        return $productsList;
    }
    
   
    public static function getProductById($params)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM product WHERE meta_url LIKE :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $params, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
        echo $params;
    }
    
    public static function getAdminProductById($params)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM product WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $params, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
        echo $params;
    }

    public static function getProdustsByIds($idsArray)
    {
        $db = Db::getConnection();
        $idsString = implode(',', $idsArray);
        $sql = "SELECT * FROM product WHERE status='1' AND id IN ($idsString)";
        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['meta_url'] = $row['meta_url'];
            $i++;
        }
        return $products;
    }

    public static function getProductsList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id,  name, price, code, status, meta_url FROM product ORDER BY id ASC');
        $productsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['status'] = $row['status'];
            $productsList[$i]['meta_url'] = $row['meta_url'];
            $i++;
        }
        return $productsList;
    }
    
    public static function getProductsCount()
    {
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM product');
         $row = $result->fetch();
         $productCount=$row[0];
         return  $productCount;
    }
   
    public static function deleteProductById($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM product WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateProductById($id, $options)
    {
        $db = Db::getConnection();
        $sql = "UPDATE product
            SET
                name = :name,
                code = :code,
                price = :price,
                description = :description,
                status = :status,
                meta_url = :meta_url,
                meta_title = :meta_title,
                meta_description = :meta_description,
                meta_keywords = :meta_keywords,
                user_use = :user_use,
                comment = :comment,
                is_recommended = :is_recommended
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
        $result->bindParam(':user_use', $options['user_use'], PDO::PARAM_INT);
        $result->bindParam(':comment', $options['comment'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        return $result->execute();
    }
    
    public static function createProduct($options)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO product '
        . '(name, code, price,'
        . 'description, status, meta_url, meta_title, meta_description, meta_keywords, user_use, comment, is_recommended)'
        . 'VALUES '
        . '(:name, :code, :price,'
        . ':description, :status, :meta_url, :meta_title, :meta_description, :meta_keywords, :user_use, :comment, :is_recommended)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_INT);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_STR);
        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
        $result->bindParam(':user_use', $options['user_use'], PDO::PARAM_INT);
        $result->bindParam(':comment', $options['comment'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_STR);
        if ($result->execute()) {
           return $db->lastInsertId();
        }
           return $result->execute();
      }

        public static function getImage($id)
        {
            $noImage = 'no-image.jpg';
            $path = '/upload/images/products/';
            $pathToProductImage = $path . $id . '.jpg';
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
                return $pathToProductImage;
            }
            return $path . $noImage;
    }

}
