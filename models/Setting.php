<?php

class Setting
{

    public static function getSetting()
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM `setting` WHERE 1';
        $result = $db->prepare($sql);
        $result->execute();
        return $result->fetch();
    }

    public static function updatesSetting($options)
    {
        $db = Db::getConnection();
        $sql = "UPDATE setting
            SET
                meta_title = :meta_title,
                meta_description = :meta_description,
                meta_keywords = :meta_keywords,
                email = :email,
                site_status = :site_status,
                status_test = :status_test
                WHERE 1";
        $result = $db->prepare($sql);
        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
        $result->bindParam(':meta_keywords',  $options['meta_keywords'], PDO::PARAM_STR);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':site_status', $options['site_status'], PDO::PARAM_INT);
        $result->bindParam(':status_test', $options['status_test'], PDO::PARAM_STR);
        return $result->execute();
    }
  }
