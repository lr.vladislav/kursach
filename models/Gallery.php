<?php

class Gallery
{
    public static function createGallery($options)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO gallery '
                . '(name, meta_url, meta_title, meta_description, meta_keywords)'
                . 'VALUES '
                . '(:name, :meta_url, :meta_title, :meta_description, :meta_keywords)';
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
        $result->bindParam(':meta_description',  $options['meta_description'], PDO::PARAM_STR);
        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
            return 0;
    }
    
    
    public static function getGalleryList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, name, meta_url FROM gallery ORDER BY id ASC');
        $galleryList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $galleryList[$i]['id'] = $row['id'];
            $galleryList[$i]['name'] = $row['name'];
            $galleryList[$i]['meta_url'] = $row['meta_url'];
           
          
            $i++;
        }
        return $galleryList;
    }
     public static function getGalleryObjList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, image, sort_order FROM gallery_object  WHERE gallery_id='.$_GET['gallery']);//ORDER BY id ASC
        $galleryObjList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $galleryObjList[$i]['id'] = $row['id'];
            $galleryObjList[$i]['image'] = $row['image'];
            $galleryObjList[$i]['sort_order'] = $row['sort_order'];
           
          
            $i++;
        }
        return $galleryObjList;
    }
    public static function getGalleryById($id)
    {
         //$object = new MetaTags; $object->setTitle("name"); 
        $db = Db::getConnection();
//        $object = MetaTags::setTitle("name");
        $sql = 'SELECT * FROM gallery WHERE  id =:id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
      
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
    
    
    
       public static function getPageGalelry()
    {
        $db = Db::getConnection();
        $sql='SELECT id,  name, meta_url FROM gallery';
        $result = $db->prepare($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $galleryList[$i]['id'] = $row['id'];
            $galleryList[$i]['name'] = $row['name'];
            $galleryList[$i]['meta_url'] = $row['meta_url'];
          
            $i++;
        }
        return $galleryList;
    }
    
  
   public static function getSiteGalleryById($id)
    {
        $db = Db::getConnection();
        $sql='SELECT id, image FROM gallery_object WHERE gallery_id='.$_GET['gallery'];
        $result = $db->prepare($sql);
    
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $i = 0;
        $photoList = array();
        while ($row = $result->fetch()) {
            $photoList[$i]['id'] = $row['id'];
            $photoList[$i]['image'] = $row['image'];
            $i++;
        }
        return $photoList;
    }
    
     public static function getSiteGalleryFirstById()
    {
         $db = Db::getConnection();
//        $object = MetaTags::setTitle("name");
        $sql = 'SELECT * FROM gallery_object WHERE gallery_id='.$_GET['gallery'];
        $result = $db->prepare($sql);
       
      
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
        
    }
    
    
    public static function getPagename(){
       //$object = new MetaTags; $object->setTitle("name"); 
        $db = Db::getConnection();
//        $object = MetaTags::setTitle("name");
        $sql = 'SELECT * FROM gallery WHERE  id ='.$_GET['gallery'];
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
      
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
    
//    public static function getArticlesAdminById($id)
//    {
//        $db = Db::getConnection();
//        $sql = 'SELECT * FROM articles WHERE id=:id';
//        $result = $db->prepare($sql);
//        $result->bindParam(':id', $id, PDO::PARAM_INT);
//        $result->setFetchMode(PDO::FETCH_ASSOC);
//        $result->execute();
//        return $result->fetch();
//    }
//    
//    public static function getTotalArticlesInCategory($articlesId)
//    {
//        $db = Db::getConnection();
//        $sql = 'SELECT count(id) AS count FROM articles WHERE status="1" AND category_id = :category_id';
//        $result = $db->prepare($sql);
//        $result->bindParam(':category_id', $articleId, PDO::PARAM_INT);
//        $result->execute();
//        $row = $result->fetch();
//        return $row['count'];
//    }
//    
//    public static function getArticlesList()
//    {
//        $db = Db::getConnection();
//        $result = $db->query('SELECT id, name, rozdil, status, meta_url FROM articles ORDER BY id ASC');
//        $articlesList = array();
//        $i = 0;
//        while ($row = $result->fetch()) {
//            $articlesList[$i]['id'] = $row['id'];
//            $articlesList[$i]['name'] = $row['name'];
//            $articlesList[$i]['rozdil'] = $row['rozdil'];
//            $articlesList[$i]['status'] = $row['status'];
//            $articlesList[$i]['meta_url'] = $row['meta_url'];
//          
//            $i++;
//        }
//        return $articlesList;
//    }
//
//    public static function getArticlesMenu()
//    {
//        $db = Db::getConnection();
//        $result = $db->query('SELECT name, rozdil, status, meta_url FROM articles WHERE status=1 AND rozdil LIKE "vidvidyvachi" ORDER BY id ASC');
//        $articlesMenu = array();
//        $i = 0;
//        while ($row = $result->fetch()) {
//            $articlesMenu[$i]['name'] = $row['name'];
//            $articlesMenu[$i]['rozdil'] = $row['rozdil'];
//            $articlesMenu[$i]['meta_url'] = $row['meta_url'];
//            $i++;
//        }
//        return $articlesMenu;
//    }
//   
//    public static function deleteArticlesById($id)
//    {
//        $db = Db::getConnection();
//        $sql = 'DELETE FROM articles WHERE id = :id';
//        $result = $db->prepare($sql);
//        $result->bindParam(':id', $id, PDO::PARAM_INT);
//        return $result->execute();
//    }
//    
//    public static function updateArticlesById($id, $options)
//    {
//        $db = Db::getConnection();
//        $sql = "UPDATE articles
//            SET 
//                user_id = :user_id, 
//                name = :name, 
//                date = :date, 
//                rozdil = :rozdil, 
//                description = :description, 
//                status = :status,
//                is_comment = :comment,
//                is_robots = :robots,
//                meta_url = :meta_url,
//                meta_title = :meta_title, 
//                meta_description = :meta_description, 
//                meta_keywords = :meta_keywords,
//                rulee = :rulee
//            WHERE id= :id";
//        $result = $db->prepare($sql);
//        $result->bindParam(':user_id', $options['user_id'], PDO::PARAM_STR);
//        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
//        $result->bindParam(':date',  $options['date'], PDO::PARAM_STR);
//        $result->bindParam(':rozdil', $options['rozdil'], PDO::PARAM_STR);
//        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
//        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
//        $result->bindParam(':comment', $options['comment'], PDO::PARAM_INT);
//        $result->bindParam(':robots', $options['robots'], PDO::PARAM_STR);
//        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
//        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
//        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
//        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
//        $result->bindParam(':rulee', $options['rulee'], PDO::PARAM_STR);
//        $result->bindParam(':id', $id, PDO::PARAM_INT);
//        return $result->execute();
//    }
//    
//    public static function createArticles($options)
//    {
//        $db = Db::getConnection();
//        $sql = 'INSERT INTO articles '
//                . '(user_id, name, date, rozdil, description, status, is_comment, is_robots, meta_url, meta_title, meta_description, meta_keywords,'
//                . 'rulee)'
//                . 'VALUES '
//                . '(:user_id, :name, :date, :rozdil, :description, :status, :comment,'
//                . ':robots, :meta_url, :meta_title, :meta_description, :meta_keywords, :rulee)';
//        $result = $db->prepare($sql);
//        $result->bindParam(':user_id', $options['user_id'], PDO::PARAM_STR);
//        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
//        $result->bindParam(':date',  $options['date'], PDO::PARAM_STR);
//        $result->bindParam(':rozdil', $options['rozdil'], PDO::PARAM_STR);
//        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
//        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
//        $result->bindParam(':comment', $options['comment'], PDO::PARAM_INT);
//        $result->bindParam(':robots', $options['robots'], PDO::PARAM_STR);
//        $result->bindParam(':meta_url', $options['meta_url'], PDO::PARAM_STR);
//        $result->bindParam(':meta_title', $options['meta_title'], PDO::PARAM_STR);
//        $result->bindParam(':meta_description', $options['meta_description'], PDO::PARAM_STR);
//        $result->bindParam(':meta_keywords', $options['meta_keywords'], PDO::PARAM_STR);
//        $result->bindParam(':rulee', $options['rulee'], PDO::PARAM_STR);
//        if ($result->execute()) {
//            return $db->lastInsertId();
//        }
//            return 0;
//    }
//    
//    public static function getArticlesCount()
//    {
//         $db = Db::getConnection();
//         $result = $db->query('SELECT COUNT(*) FROM articles');
//         $row = $result->fetch();
//         $articlesCount=$row[0];
//         return  $articlesCount;
//    }
    
}