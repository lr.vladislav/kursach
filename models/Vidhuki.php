<?php

class Vidhuki
    {
    
    const SHOW_BY_DEFAULT = 8;
    public static function getTotalVidhukiInCategory()
    {
        $db = Db::getConnection();
        $sql = 'SELECT count(id) AS count FROM vidhuki WHERE status="1" ';
        $result = $db->prepare($sql);
        $result->execute();
        $row = $result->fetch();
        return $row['count'];
    }

    public static function getVidhukiList($page = 1)
    {
        $limit = Vidhuki::SHOW_BY_DEFAULT;
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        $db = Db::getConnection();
        $sql = 'SELECT id, date, name, text_comment FROM vidhuki '
                . 'WHERE status = 1 '
                . 'ORDER BY id DESC LIMIT :limit OFFSET :offset';
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->execute();
        $i = 0;
        $vidhuki = array();
        while ($row = $result->fetch()) {
            $vidhuki[$i]['id'] = $row['id'];
            $vidhuki[$i]['date'] = $row['date'];
            $vidhuki[$i]['name'] = $row['name'];
            $vidhuki[$i]['text_comment'] = $row['text_comment'];
            $i++;
        }
        return $vidhuki;
    }
     public static function getAllVidhuki()
    {
        $db = Db::getConnection();
        $sql = 'SELECT id, date, name, text_comment, status FROM vidhuki '
                . 'ORDER BY id DESC';
        $result = $db->prepare($sql);
        $result->execute();
         $i = 0;
        $vidhukiListt = array();
        while ($row = $result->fetch()) {
            $vidhukiListt[$i]['id'] = $row['id'];
            $vidhukiListt[$i]['date'] = $row['date'];
            $vidhukiListt[$i]['name'] = $row['name'];
            $vidhukiListt[$i]['text_comment'] = $row['text_comment'];
            $vidhukiListt[$i]['status'] = $row['status'];
            $i++;
        }
        return $vidhukiListt;
    }
     public static function getVidhukCount()
    {
         $db = Db::getConnection();
         $result = $db->query('SELECT COUNT(*) FROM vidhuki');
         $row = $result->fetch();
         $vidhukCount=$row[0];
         return  $vidhukCount;
    }
     public static function getVidhukAdminById($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM vidhuki WHERE id=:id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
     public static function updateVidhukById($id, $options)
    {
        $db = Db::getConnection();
        $sql = "UPDATE vidhuki
            SET 
                name = :name, 
                date = :date, 
                text_comment = :text_comment, 
                status = :status
           WHERE id= :id";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':date', $options['date'], PDO::PARAM_STR);
        $result->bindParam(':text_comment',  $options['text_comment'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
     public static function deleteVidhukiById($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM vidhuki WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    
}
   
