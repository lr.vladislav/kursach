<?php


class AdminArticlesController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $articlesList = Articles::getArticlesList();
        $articlesCount = Articles::getArticlesCount();
        require_once(ROOT . '/views/admin_articles/index.php');
        return true;
    }
    
    public function actionCreate()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $rozdilList = Rozdil::getRozdilListAdmin();
        $articlesCount = Articles::getArticlesCount();
        if (isset($_POST['submit'])) {
            $options['user_id'] = $_SESSION['user'];
            $options['name'] = $_POST['name'];
            $options['date'] = date("m.d.y-H:i");
            $options['rozdil'] = $_POST['rozdil'];
            $options['description'] = $_POST['description'];
            $options['status'] = $_POST['status'];
            $options['comment'] = $_POST['comment'];
            $options['robots'] = $_POST['robots'];
            $options['meta_url'] = $_POST['meta_url'];
            $options['meta_title'] = $_POST['meta_title'];
            $options['meta_description'] = $_POST['meta_description'];
            $options['meta_keywords'] = $_POST['meta_keywords'];
            $options['rulee'] = $_POST['rule'];
            $errors = false;
            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть обов&#39;язкові поля';
            }
            if ($errors == false) {
                 $id = Articles::createArticles($options);
                header("Location: /admin/articles");
            }
        }
        require_once(ROOT . '/views/admin_articles/create.php');
        return true;
    }
    
    public function actionUpdate($id)
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $rozdilList = Rozdil::getRozdilListAdmin();
        $articlesCount = Articles::getArticlesCount();
        $articles = Articles::getArticlesAdminById($id);
        if (isset($_POST['submit'])) {
            $options['user_id'] = $_SESSION['user'];
            $options['name'] = $_POST['name'];
            $options['date'] = date("m.d.y-H:i");
            $options['rozdil'] = $_POST['rozdil'];
            $options['description'] = $_POST['description'];
            $options['status'] = $_POST['status'];
            $options['comment'] = $_POST['comment'];
            $options['robots'] = $_POST['robots'];
            $options['meta_url'] = $_POST['meta_url'];
            $options['meta_title'] = $_POST['meta_title'];
            $options['meta_description'] = $_POST['meta_description'];
            $options['meta_keywords'] = $_POST['meta_keywords'];
            $options['rulee'] = $_POST['rule'];
            $errors = false;
            if (Articles::updateArticlesById($id, $options)) {
              
            }
            header("Location: /admin/articles");
        }
        require_once(ROOT . '/views/admin_articles/update.php');
        return true;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $articlesCount = Articles::getArticlesCount();
        $articles = Articles::getArticlesAdminById($id);
        if (isset($_POST['submit'])) {
            Articles::deleteArticlesById($id);
            header("Location: /admin/articles");
        }
        require_once(ROOT . '/views/admin_articles/delete.php');
        return true;
    }

}
