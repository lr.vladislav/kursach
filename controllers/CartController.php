<?php

class CartController
{

    public function actionAdd($id)
    {
        Cart::addProduct($id);
        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: $referrer");
    }

    public function actionAddAjax($id)
    {
        echo Cart::addProduct($id);
        return true;
    }

    public function actionDelete($id)
    {
        Cart::deleteProduct($id);
        header("Location: /cart");
    }

    public function actionIndex()
    {
        $setting=Setting::getSetting();
        $productsInCart = Cart::getProducts();
        if ($productsInCart) {
            $productsIds = array_keys($productsInCart);
            $products = Product::getProdustsByIds($productsIds);
            $totalPrice = Cart::getTotalPrice($products);
        }
        require_once(ROOT . '/views/cart/index.php');
        return true;
    }

    public function actionCheckout()
    {
        $setting=Setting::getSetting();
        $productsInCart = Cart::getProducts();
        if ($productsInCart == false) {
            header("Location: /");
        }
        $productsIds = array_keys($productsInCart);
        $products = Product::getProdustsByIds($productsIds);
        $totalPrice = Cart::getTotalPrice($products);
        $totalQuantity = Cart::countItems();
        $userName = false;
        $userPhone = false;
        $userAdress = false;
        $userComment = false;
        $result = false;
        if (!User::isGuest()) {
            $userId = User::checkLogged();
            $user = User::getUserById($userId);
            $userName = $user['name'];
        } else {
            $userId = false;
        }
        if (isset($_POST['submit'])) {
            $userName = $_POST['user_name'];
            $userPhone = $_POST['user_phone'];
            $userAdress = $_POST['user_adress'];
            $userHome = $_POST['dostavka'];
            $userMany =$_POST['many'];
            $userComment = $_POST['user_comment'];
            $errors = false;
            if (!User::checkName($userName)) {
                $errors[] = 'Неправильное имя';
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = 'Неправильный телефон';
            }


            if ($errors == false) {
                $result = Order::save($userName, $userPhone, $userAdress,$userHome, $userMany, $userComment, $userId, $productsInCart);
                $resultt = Order::saves($userName, $userPhone, $userAdress,$userHome, $userMany, $userComment, $userId, $productsInCart);
                if ($result) {

                    Cart::clear();
                }
            }
        }
        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }

}
