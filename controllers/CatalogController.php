<?php

class CatalogController
{
    public function actionIndex()
    {

    }
    public function actionCategory($articlesId)
    {
        $setting=Setting::getSetting();
        $pagename = Category::getPagename();
        $categoryArticles = Articles::getArticlesListByCategory($articlesId);
        require_once(ROOT . '/views/catalog/category.php');
        return true;
    }
    public function actionView($articlesId)
    {
         $setting=Setting::getSetting();
        $pagename = Category::getPagename();
        $categoryArticles = Articles::getArticlesListByCategory($articlesId);
        require_once(ROOT . '/views/catalog/category.php');
        return true;
    }
}
