<?php

class SiteController
{

    public function actionIndex()
    {
        $setting=Setting::getSetting();
        $categories = Category::getCategoriesList();
        $rightmenu = Articles::getArticlesMenu();
        $latestProducts = Product::getLatestProducts(6);
        require_once(ROOT . '/views/site/index.php');
        return true;
    }
    public function actionAsk(){
      require_once(ROOT . '/views/ask/index.php');
      return true;
    }
    public function actionFeedback()
    {
        $userEmail = false;
        $userText = false;
        $result = false;
        if (isset($_POST['submit'])) {
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];
            $errors = false;
            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }
            if ($errors == false) {
                $adminEmail = 'php.start@mail.ru';
                $message = "Текст: {$userText}. От {$userEmail}";
                $subject = 'Тема письма';
                $result = mail($adminEmail, $subject, $message);
                $result = true;
            }
        }
        require_once(ROOT . '/views/site/feedback.php');
        return true;
    }

    public function actionAbout()
    {
       require_once(ROOT . '/views/site/about.php');
        return true;
    }

}
