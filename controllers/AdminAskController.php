<?
class AdminAskController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $askiListt = Ask::getAllAsk();
        $askCount = Ask::getAskCount();
        require_once(ROOT . '/views/admin_ask/index.php');
        return true;
    }
     public function actionUpdate($id)
    {
        self::checkAdmin();
         $orderAlert = Order::getOrderAlert();
        $vidhuk = Vidhuki::getVidhukAdminById($id);
        $vidhukCount = Vidhuki::getVidhukCount();
            if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['date'] = $_POST['date'];
            $options['text_comment'] = $_POST['text_comment'];
            $options['status'] = $_POST['status'];
            $errors = false;
            if (Vidhuki::updateVidhukById($id, $options)) {

            }
            header("Location: /admin/vidhuk");
        }
        require_once(ROOT . '/views/admin_vidhuk/update.php');
        return true;
    }
      public function actionDelete($id)
    {
        self::checkAdmin();
          $orderAlert = Order::getOrderAlert();
       $vidhuk = Vidhuki::getVidhukAdminById($id);
        $vidhukCount = Vidhuki::getVidhukCount();
        if (isset($_POST['submit'])) {
            Vidhuki::deleteVidhukiById($id);
            header("Location: /admin/vidhuk");
        }
        require_once(ROOT . '/views/admin_vidhuk/delete.php');
        return true;
    }
}
?>
