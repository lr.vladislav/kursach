<?php


class VidhukiController
{
    public static function actionIndex($page = 1)
    {
        //$setting=Setting::getSetting();
        $vidhuki = Vidhuki::getVidhukiList($page);
        $total = Vidhuki::getTotalVidhukiInCategory();
        $pagination = new Pagination($total, $page, Vidhuki::SHOW_BY_DEFAULT, 'page-');
        require_once(ROOT . '/views/vidhuki/index.php');
        return true;
    }
}
 
