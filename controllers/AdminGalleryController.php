<?php

/**
 * Контроллер AdminProductController
 * Управление товарами в админпанели
 */
class AdminGalleryController extends AdminBase
{

    /**
     * Action для страницы "Управление товарами"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();

        // Получаем список товаров
        $galleryList = Gallery::getGalleryList();

        // Подключаем вид
        require_once(ROOT . '/views/admin_gallery/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить товар"
     */
    public function actionCreate()
    {
        self::checkAdmin();
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $options['name'] = $_POST['name'];
            $options['meta_url'] = $_POST['meta_url'];
            $options['meta_title'] = $_POST['meta_title'];
            $options['meta_description'] = $_POST['meta_description'];
            $options['meta_keywords'] = $_POST['meta_keywords'];
            $errors = false;

            // При необходимости можно валидировать значения нужным образом
            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заполните поля';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Добавляем новый товар
                $id = Gallery::createGallery($options);

                 

                // Перенаправляем пользователя на страницу управлениями товарами
                header("Location: /admin/photomaterial");
            }
        }

        // Подключаем вид
        require_once(ROOT . '/views/admin_gallery/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать товар"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

//        // Получаем список категорий для выпадающего списка
//        $categoriesList = Category::getCategoriesListAdmin();
//
//        // Получаем данные о конкретном заказе
        $gallery = Gallery::getGalleryById($id);
        $galleryobg =  Gallery::getGalleryObjList();
//
//        // Обработка формы
//        if (isset($_POST['submit'])) {
//            // Если форма отправлена
//            // Получаем данные из формы редактирования. При необходимости можно валидировать значения
//            $options['name'] = $_POST['name'];
//            $options['code'] = $_POST['code'];
//            $options['price'] = $_POST['price'];
//            $options['category_id'] = $_POST['category_id'];
//            $options['brand'] = $_POST['brand'];
//            $options['availability'] = $_POST['availability'];
//            $options['description'] = $_POST['description'];
//            $options['is_new'] = $_POST['is_new'];
//            $options['is_recommended'] = $_POST['is_recommended'];
//            $options['status'] = $_POST['status'];
//
//            // Сохраняем изменения
//            if (Product::updateProductById($id, $options)) {
//
//
//                // Если запись сохранена
//                // Проверим, загружалось ли через форму изображение
//                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
//
//                    // Если загружалось, переместим его в нужную папке, дадим новое имя
//                   move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
//                }
//            }
//
//            // Перенаправляем пользователя на страницу управлениями товарами
//            header("Location: /admin/gallery");
//        }

        // Подключаем вид
        require_once(ROOT . '/views/admin_gallery/edit.php');
        return true;
    }

    /**
     * Action для страницы "Удалить товар"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем товар
            Product::deleteProductById($id);

            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /admin/product");
        }

        // Подключаем вид
        require_once(ROOT . '/views/admin_product/delete.php');
        return true;
    }

}
