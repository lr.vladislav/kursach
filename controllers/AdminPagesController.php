<?php


class AdminPagesController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $pagesList = Pages::getPagesList();
        $pagesCount = Pages::getPagesCount();
        require_once(ROOT . '/views/admin_pages/index.php');
        return true;
    }
    
    public function actionCreate()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $pagesCount = Pages::getPagesCount();
        if (isset($_POST['submit'])) {
            $options['user_id'] = $_SESSION['user'];
            $options['name'] = $_POST['name'];
            $options['date'] = date("m.d.y-H:i");
            $options['description'] = $_POST['description'];
            $options['status'] = $_POST['status'];
            $options['comment'] = $_POST['comment'];
            $options['robots'] = $_POST['robots'];
            $options['meta_url'] = $_POST['meta_url'];
            $options['meta_title'] = $_POST['meta_title'];
            $options['meta_description'] = $_POST['meta_description'];
            $options['meta_keywords'] = $_POST['meta_keywords'];
            $options['rulee'] = $_POST['rule'];
            $errors = false;
            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть обов&#39;язкові поля';
            }
            if ($errors == false) {
                 $id = Pages::createPages($options);
                header("Location: /admin/pages");
            }
        }
        require_once(ROOT . '/views/admin_pages/create.php');
        return true;
    }
    
    public function actionUpdate($id)
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $pagesCount = Pages::getPagesCount();
        $pages = Pages::getPagesAdminById($id);
        if (isset($_POST['submit'])) {
            $options['user_id'] = $_SESSION['user'];
            $options['name'] = $_POST['name'];
            $options['date'] = date("m.d.y-H:i");
            $options['description'] = $_POST['description'];
            $options['status'] = $_POST['status'];
            $options['comment'] = $_POST['comment'];
            $options['robots'] = $_POST['robots'];
            $options['meta_url'] = $_POST['meta_url'];
            $options['meta_title'] = $_POST['meta_title'];
            $options['meta_description'] = $_POST['meta_description'];
            $options['meta_keywords'] = $_POST['meta_keywords'];
            $options['rulee'] = $_POST['rule'];
            $errors = false;
            if (Pages::updatePagesById($id, $options)) {
              
            }
            header("Location: /admin/pages");
        }
        require_once(ROOT . '/views/admin_pages/update.php');
        return true;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $pagesCount = Pages::getPagesCount();
        $pages = Pages::getPagesAdminById($id);
        if (isset($_POST['submit'])) {
            Pages::deletePagesById($id);
            header("Location: /admin/pages");
        }
        require_once(ROOT . '/views/admin_pages/delete.php');
        return true;
    }

}
