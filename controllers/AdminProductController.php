<?php

/**
 * Контроллер AdminProductController
 * Управление товарами в админпанели
 */
class AdminProductController extends AdminBase
{

    /**
     * Action для страницы "Управление товарами"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();
$orderAlert = Order::getOrderAlert();
        // Получаем список товаров
        $productsList = Product::getProductsList();
        $productsCount = Product::getProductsCount();
        // Подключаем вид
        require_once(ROOT . '/views/admin_product/index.php');
        return true;
    }

    /**
     * Action для страницы "Добавить товар"
     */
    public function actionCreate()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        //$categoriesList = Category::getCategoriesListAdmin();
        $productsCount = Product::getProductsCount();
        if (isset($_POST['submit'])) {
              $options['name'] = $_POST['name'];
              $options['code'] = $_POST['code'];
              $options['price'] = $_POST['price'];
              $options['description'] = $_POST['description'];
              $options['status'] = $_POST['status'];
              $options['meta_url'] = $_POST['meta_url'];
              $options['meta_title'] = $_POST['meta_title'];
              $options['meta_description'] = $_POST['meta_description'];
              $options['meta_keywords'] = $_POST['meta_keywords'];
              $options['user_use'] = $_POST['user_use'];
              $options['comment'] = $_POST['comment'];
              $options['is_recommended'] = $_POST['is_recommended'];
              $errors = false;
              if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть необхідні поля';
              }
                if ($errors == false) {
                $id = Product::createProduct($options);
                if ($id) {
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
                    }
                };
                header("Location: /admin/product");
            }
        }

        // Подключаем вид
        require_once(ROOT . '/views/admin_product/create.php');
        return true;
    }

    /**
     * Action для страницы "Редактировать товар"
     */
    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();
$orderAlert = Order::getOrderAlert();
        $productsCount = Product::getProductsCount();
        // Получаем данные о конкретном заказе
        $product = Product::getAdminProductById($id);
     
        // Обработка формы
        if (isset($_POST['submit'])) {
              $options['name'] = $_POST['name'];
              $options['code'] = $_POST['code'];
              $options['price'] = $_POST['price'];
              $options['description'] = $_POST['description'];
              $options['status'] = $_POST['status'];
              $options['meta_url'] = $_POST['meta_url'];
              $options['meta_title'] = $_POST['meta_title'];
              $options['meta_description'] = $_POST['meta_description'];
              $options['meta_keywords'] = $_POST['meta_keywords'];
              $options['user_use'] = $_POST['user_use'];
              $options['comment'] = $_POST['comment'];
              $options['is_recommended'] = $_POST['is_recommended'];
            // Сохраняем изменения
            if (Product::updateProductById($id, $options)) {


                // Если запись сохранена
                // Проверим, загружалось ли через форму изображение
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {

                    // Если загружалось, переместим его в нужную папке, дадим новое имя
                   move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
                }
            }

            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /admin/product");
        }

        // Подключаем вид
        require_once(ROOT . '/views/admin_product/update.php');
        return true;
    }

    /**
     * Action для страницы "Удалить товар"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $productsCount = Product::getProductsCount();
        $product = Product::getAdminProductById($id);
        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем товар
            Product::deleteProductById($id);

            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /admin/product");
        }

        // Подключаем вид
        require_once(ROOT . '/views/admin_product/delete.php');
        return true;
    }

}
