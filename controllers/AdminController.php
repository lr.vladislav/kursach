<?php

/**
 * Контроллер AdminController
 * Главная страница в админпанели
 */
class AdminController extends AdminBase
{
    /**
     * Action для стартовой страницы "Панель администратора"
     */
    public function actionIndex()
    {
        // Проверка доступа
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
         
         $orderStats=Order::getOrdersListStats();
        
       $orderStatsCount=Order::getOrderStats();
        $lastorder=Order::getOrdersListMain();
       
        // Подключаем вид
        require_once(ROOT . '/views/admin/index.php');
        return true;
    }

}
