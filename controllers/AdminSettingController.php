<?php


class AdminSettingController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $orderAlert = Order::getOrderAlert();
        $setting = Setting::getSetting();
        if (isset($_POST['submit'])) {
            $options['meta_title'] = $_POST['meta_title'];
            $options['meta_description'] = $_POST['meta_description'];
            $options['meta_keywords'] = $_POST['meta_keywords'];
            $options['email'] = $_POST['email'];
            $options['site_status'] = $_POST['site_status'];
            $options['status_test'] = $_POST['status_test'];
            $errors = false;
            if (Setting::updatesSetting($options)) {

            }
            header("Location: /admin/setting");
        }
        require_once(ROOT . '/views/admin_setting/index.php');
        return true;
    }

}
