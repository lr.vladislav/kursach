<?
// title
if(isset($articles['meta_title'])){
   echo "<title>".$articles['meta_title']."</title>";
}
elseif(isset($product['meta_title'])){
    echo "<title>".$product['meta_title']."</title>";
}
elseif(isset($pages['meta_title'])){
    echo "<title>".$pages['meta_title']."</title>";
}elseif(isset($pages['meta_title'])){
    echo "<title>".$pages['meta_title']."</title>";
}
elseif(isset($setting['meta_title'])){
    echo "<title>".$setting['meta_title']."</title>";
}
elseif($_SERVER['REQUEST_URI']=='/vidhuki'){
    echo "<title>Відгуки про продукцію «Унібіол» та «Біоназол» | «АрборВіте»</title>";
} 
elseif($_SERVER['REQUEST_URI']=='/cart'){
    echo "<title>Оформлення замовлення | «АрборВіте»</title>";
}
// deskription
if(isset($articles['meta_description'])){
   echo "<meta name='description' content='".$articles['meta_description']."'/>";
}
elseif(isset($product['meta_description'])){
   echo "<meta name='description' content='".$product['meta_description']."'/>";
}
elseif(isset($pages['meta_description'])){
   echo "<meta name='description' content='".$pages['meta_description']."'/>";
}
elseif(isset($setting['meta_description'])){
   echo "<meta name='description' content='".$setting['meta_description']."'/>";
}
//elseif(isset($metatitle)){
//   echo "<meta name='description' content='".$metatitle."'/>";
//}
elseif($_SERVER['REQUEST_URI']=='/vidhuki'){
    $metatitle="Відгуки про продукцію «Унібіол» та «Біоназол»  на офіційному сайті «АрборВіте»";
      echo "<meta name='description' content='".$metatitle."'/>";
}
// keywords
if(isset($articles['meta_keywords'])){
   echo "<meta name='keywords' content='".$articles['meta_keywords']."'/>";
}
elseif(isset($product['meta_keywords'])){
   echo "<meta name='keywords' content='".$product['meta_keywords']."'/>";
}
elseif($_SERVER['REQUEST_URI']=='/vidhuki'){
   echo "<meta name='keywords' content='Унібіол, біоназол, унибиол, бионазол'/>";
}
elseif(isset($pages['meta_keywords'])){
   echo "<meta name='keywords' content='".$pages['meta_keywords']."'/>";
}
elseif(isset($setting['meta_keywords'])){
   echo "<meta name='keywords' content='".$setting['meta_keywords']."'/>";
}
?>
